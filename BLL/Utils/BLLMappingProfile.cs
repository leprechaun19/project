﻿using AutoMapper;
using BLL.DTO;
using DAL.Entities;
using DAL.Entities.JoinEntities;

namespace BLL.Utils
{
    public class BLLMappingProfile : Profile
    {
        /// <summary>
        /// Creates map for a mapping.
        /// </summary>
        public BLLMappingProfile()
        {
            CreateMap<UserPersonalProfileDTO, Users>();
            //
            CreateMap<Users, UserInfoDTO>();
            //
            CreateMap<UserCreateDTO, Users>();
            //
            CreateMap<Users, UserProfileDTO>();
            //
            CreateMap<Users, UserUpdateDTO>();
            //
            CreateMap<UserUpdateDTO, Users>();
             //
             CreateMap<Housing, HousingIndexDTO>();
            //
            CreateMap<Housing, HousingInfoDTO>();
            //
            CreateMap<HousingCreateDTO, Housing>();
            //
            CreateMap<Housing, HousingUpdateDTO>();
            //
            CreateMap<Housing, UserHousingDTO>();
            //
            CreateMap<HousingUpdateDTO, Housing>();
            //
            CreateMap<ScheduleOfBooking, ScheduleOfBookingDTO>();
            //
            CreateMap<Favourites, FavouriteCreateDTO>();
            //
            CreateMap<FavouriteCreateDTO, Favourites>();
            //
            CreateMap<CommentWithUserName, CommentHousingDTO>()
                .ForMember(c => c.Comment, opt => opt.MapFrom(model => model.Text))
                .ForMember(c => c.CreationDate, opt => opt.MapFrom(model => model.CreationDate))
                .ForMember(c => c.Evaluation, opt => opt.MapFrom(model => model.Evaluation))
                .ForMember(c => c.UserId, opt => opt.MapFrom(model => model.UserId));
            //
            CreateMap<CommentCreateDTO, Comments>();
            //
            CreateMap<Comments, CommentUpdateDTO>();
            //
            CreateMap<CommentUpdateDTO, Comments>();
            //
            CreateMap<CommentWithUserName, CommentHousingDTO>();
            //
            CreateMap<CommentWithHousingInfo, CommentProfileDTO>()
                .ForPath(cP => cP.Housing.Id, opt => opt.MapFrom(cW => cW.HousingId))
                .ForPath(cP => cP.Housing.TypeOfHousing, opt => opt.MapFrom(cW => cW.TypeOfHousing))
                .ForPath(cP => cP.Housing.Address, opt => opt.MapFrom(cW => cW.Address))
                .ForPath(cP => cP.Housing.AdHeadline, opt => opt.MapFrom(cW => cW.AdHeadline))
                .ForPath(cP => cP.Housing.PriceNight, opt => opt.MapFrom(cW => cW.PriceNight));
            //
            CreateMap<BookingWithHousingInfo, BookingInfoDTO>()
                .ForPath(bW => bW.Housing.Id, opt => opt.MapFrom(bInfo => bInfo.HousingId))
                .ForPath(bW => bW.Housing.MainImgUrl, opt => opt.MapFrom(bInfo => bInfo.MainImgUrl))
                .ForPath(bW => bW.Housing.TypeOfHousing, opt => opt.MapFrom(bInfo => bInfo.TypeOfHousing))
                .ForPath(bW => bW.Housing.Address, opt => opt.MapFrom(bInfo => bInfo.Address))
                .ForPath(bW => bW.Housing.AdHeadline, opt => opt.MapFrom(bInfo => bInfo.AdHeadline));
            //
            CreateMap<Bookings, BookingIndexDTO>();
            //
            CreateMap<BookingCreateDTO, Bookings>();
            //
            CreateMap<BookingUpdateDTO, Bookings>();
        }
    }
}
