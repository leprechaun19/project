﻿using AutoMapper;
using DapperExtensions;
using BLL.DTO;
using BLL.Interfaces.IService;
using DAL.Entities;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class FavouriteService : IFavouriteService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public FavouriteService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        ///<inheritdoc/>
        public async Task Create(FavouriteCreateDTO item)
        {
            Favourites favourites = _mapper.Map<FavouriteCreateDTO, Favourites>(item);
            favourites.CreationDate = DateTime.Today;

            await _unitOfWork.Favourites.Create(favourites);
            _unitOfWork.SaveChanges();
        }

        ///<inheritdoc/>
        public async Task Delete(int favouriteId)
        {
            var favourite = await _unitOfWork.Favourites.GetById(favouriteId);

            if(favourite != null)
            {
                await _unitOfWork.Favourites.Delete(favourite);
                _unitOfWork.SaveChanges();
            }
        }

        ///<inheritdoc/>
        public async Task<bool> IsFavourite(int userId, int housingId)
        {
            var predicateGroup = new PredicateGroup() { Operator = GroupOperator.And, Predicates = new List<IPredicate>() };
            predicateGroup.Predicates.Add(Predicates.Field<Favourites>(favourite => favourite.HousingId, Operator.Eq, housingId));
            predicateGroup.Predicates.Add(Predicates.Field<Favourites>(favourite => favourite.UserId, Operator.Eq, userId));

            var favsCount = await _unitOfWork.Favourites.GetByPredicateGroupCount(predicateGroup);
            bool result = favsCount > 0;

            return result;
        }
    }
}
