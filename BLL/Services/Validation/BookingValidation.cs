﻿using BLL.DTO;
using BLL.Interfaces.Validation;
using Common.Enums;
using Common.Error;
using DAL.Interfaces;
using Serilog;
using System;
using System.Threading.Tasks;

namespace BLL.Services.Validation
{
    public class BookingValidation : IBookingValidation
    {
        private readonly IUnitOfWork _unitOfWork;

        public BookingValidation(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        ///<inheritdoc/>
        public async Task<ErrorModel> ValidateBookingToDelete(int bookingId, int userId)
        {
            var booking = await _unitOfWork.Bookings.GetById(bookingId);
            if (booking.UserId != userId)
            {
                Log.Error("Validation.ValidateBookingToDelete - isn't owner of booking. ");
                return ValidateHelper.IsNotOwnerBooking;
            }

            if (booking.Status == BookingStatus.BookedPaid)
            {
                Log.Error("Validation.ValidateBookingToDelete - booking is paid, can't delete. ");
                return ValidateHelper.DeleteBookingPaid;
            }

            return null;
        }

        ///<inheritdoc/>
        public async Task<ErrorModel> ValidateBookingToCreate(BookingCreateDTO model, DateTime start, DateTime end)
        {
            var housing = await _unitOfWork.Houses.GetById(model.HousingId);
            if (housing.NumberOfPossibleGuests < model.Adults + model.ChildrenFrom6To17)
            {
                Log.Error("Validation.ValidateBookingToCreate  -  the number of people is invalid.");
                return ValidateHelper.NumberPeopleInvalid;
            }

            if (housing.UserId == model.UserId)
            {
                Log.Error(string.Concat("Validation.ValidateBookingToCreate - owner(Id - ", model.UserId, ") want to booking his housing. "));
                return ValidateHelper.OwnerBookingHisHousing;
            }
            var datesValid = await _unitOfWork.Houses.IsDatesFree(model.HousingId, start, end);
            if (!datesValid)
            {
                Log.Error(string.Concat("Validation.ValidateBookingToCreate  -  the dates for housing(Id - ", model.HousingId, ") is not free."));
                return ValidateHelper.BookedYet;
            }
            return null;
        }

        ///<inheritdoc/>
        public async Task<ErrorModel> CheckDatesIsFree(int bookingId)
        {
            var booking = await _unitOfWork.Bookings.GetById(bookingId);
            var isFree = await _unitOfWork.Houses.IsDatesFree(booking.HousingId, booking.StartDate, booking.EndDate);
            if (!isFree)
            {
                Log.Error(string.Concat("Validation.CheckDatesIsFree  -  the dates for housing(Id - ", booking.HousingId, ") is not free."));
                return ValidateHelper.BookedYet;
            }

            return null;
        }
    }
}
