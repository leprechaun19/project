﻿using DapperExtensions;
using BLL.Interfaces.Validation;
using Common.Error;
using DAL.Entities;
using DAL.Interfaces;
using Serilog;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RentalHousing.BLL.Services.Validation
{
    public class FavouriteValidation : IFavouriteValidation
    {
        private readonly IUnitOfWork _unitOfWork;

        public FavouriteValidation(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        ///<inheritdoc/>
        public async Task<ErrorModel> ValidateToCreate(int userId, int housingId)
        {
            if (housingId == 0 || userId == 0)
            {
                Log.Error("FavouriteValidation.ValidateToCreate - the user enter null housing Id. ");
                return ValidateHelper.InvalidData;
            }

            var housing = await _unitOfWork.Houses.GetById(housingId);
            if (housing.Deleted)
            {
                Log.Error("FavouriteValidation.ValidateToCreate - the user enter deleted housing Id. ");
                return ValidateHelper.InvalidData;
            }

            if (housing.UserId == userId)
            {
                Log.Error(string.Concat("FavouriteValidation.ValidateToCreate - this user(Id - ", userId, ") want to add to favs his house. "));
                return ValidateHelper.AddToFavSelf;
            }

            var predicateGroup = new PredicateGroup() { Operator = GroupOperator.And, Predicates = new List<IPredicate>() };
            predicateGroup.Predicates.Add(Predicates.Field<Favourites>(favourite => favourite.HousingId, Operator.Eq, housingId));
            predicateGroup.Predicates.Add(Predicates.Field<Favourites>(favourite => favourite.UserId, Operator.Eq, userId));

            var favsCount = await _unitOfWork.Favourites.GetByPredicateGroupCount(predicateGroup);
            if (favsCount != 0)
            {
                Log.Error("FavouriteValidation.ValidateToCreate - the user want add house to favourites the two times. ");
                return ValidateHelper.FavExistYet;
            }

            return null;
        }
    }
}
