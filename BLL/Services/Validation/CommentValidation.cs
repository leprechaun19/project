﻿using BLL.Interfaces.Validation;
using Common.Enums;
using Common.Error;
using DAL.Interfaces;
using Serilog;
using System;
using System.Threading.Tasks;

namespace BLL.Services.Validation
{
    public class CommentValidation : ICommentValidation
    {
        private readonly IUnitOfWork _unitOfWork;

        public CommentValidation(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        ///<inheritdoc/>
        public async Task<ErrorModel> ValidateCommentToAdd(int bookingId, int userId, int housingId)
        {
            var booking = await _unitOfWork.Bookings.GetById(bookingId);
            if (booking.EndDate > DateTime.Now)
            {
                Log.Error(string.Concat("Validation.ValidateCommentToAdd - user(Id - ", userId, ") want to add comment for the booking that don't ended. "));
                return ValidateHelper.BookingNotEndedForComment;
            }

            if (booking.Status == BookingStatus.CancelPayment)
            {
                Log.Error(string.Concat("Validation.ValidateBookingToDelete - the user(Id - ", userId, ") want to add comment for the booking isn't paid. "));
                return ValidateHelper.BookingNotPaidForComment;
            }
            var isCreated = await _unitOfWork.Comments.IsCommentCreated(userId, housingId);
            if (isCreated)
            {
                Log.Error(string.Concat("Validation.ValidateBookingToDelete - this user(Id - ", userId, ") want create comment again. "));
                return ValidateHelper.CommentCreated;
            }

            return null;
        }
    }
}
