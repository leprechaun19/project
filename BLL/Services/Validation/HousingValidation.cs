﻿using DapperExtensions;
using BLL.Interfaces.Validation;
using Common.Error;
using DAL.Entities;
using DAL.Interfaces;
using Serilog;
using System.Threading.Tasks;

namespace BLL.Services.Validation
{
    public class HousingValidation : IHousingValidation
    {
        private readonly IUnitOfWork _unitOfWork;

        public HousingValidation(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        ///<inheritdoc/>
        public async Task<ErrorModel> ValidateHousingToDelete(int housingId, int userId)
        {
            var housing = await _unitOfWork.Houses.GetById(housingId);
            if (housing.UserId != userId)
            {
                Log.Error(string.Concat("Validation.ValidateHousingToDelete - the user with id - ", userId, 
                    " isn't owner of housing with id - ", housingId, "."));
                return ValidateHelper.IsNotOwnerHousing;
            }

            return null;
        }

        ///<inheritdoc/>
        public async Task<ErrorModel> ValidateHousingToBooking(int housingId)
        {
            if (housingId == 0)
            {
                Log.Error("Validation.ValidateHousingToGet - the user enter null housing Id. ");
                return ValidateHelper.InvalidData;
            }

            var housingExist = await _unitOfWork.Houses.GetByPredicateCount(
                Predicates.Field<Housing>(favourite => favourite.Id, Operator.Eq, housingId));
            if (housingExist == 0)
            {
                Log.Error(string.Concat("Validation.ValidateHousingToGet - the requested housing(Id - ", housingId, ") is not exist. "));
                return ValidateHelper.HousingDeleted;
            }

            var housing = await _unitOfWork.Houses.GetById(housingId);
            if (housing.Deleted)
            {
                Log.Error(string.Concat("Validation.ValidateHousingToGet - the requested housing(Id - ", housingId, ") is deleted. "));
                return ValidateHelper.HousingDeleted;
            }

            return null;
        }
    }
}
