﻿using DapperExtensions;
using BLL.DTO;
using BLL.Interfaces.Validation;
using Common.Error;
using Common.Interfaces;
using DAL.Entities;
using DAL.Interfaces;
using Serilog;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services.Validation
{
    public class UserValidation : IUserValidation
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IPasswordManager _passwordHasher;

        public UserValidation(IUnitOfWork unitOfWork, IPasswordManager passwordHasher)
        {
            _unitOfWork = unitOfWork;
            _passwordHasher = passwordHasher;
        }

        ///<inheritdoc/>
        public async Task<ErrorModel> ValidateOldPassword(string oldPassword, int userId)
        {
            var user = await _unitOfWork.Users.GetById(userId);
            var hashFromModel = _passwordHasher.GetPasswordSaltHash(oldPassword, user.DynamicSalt);
            if (!hashFromModel.Equals(user.PasswordHash))
            {
                Log.Error("Validation.ValidateOldPassword - the user entered old password that not valid.");
                return ValidateHelper.InvalidOldPassword;
            }

            return null;
        }

        ///<inheritdoc/>
        public async Task<ErrorModel> ValidateUsersRegisterData(UserCreateDTO model)
        {
            var usersWithSuchPhone = await _unitOfWork.Users.GetByPredicateCount(
                Predicates.Field<Users>(user => user.Phone, Operator.Eq, model.Phone));
            if (usersWithSuchPhone != 0)
            {
                Log.Error("Validation.ValidateUsersRegisterData - the phone is not unique.");
                return ValidateHelper.PhoneExist;
            }

            var usersWithSuchEmail = await _unitOfWork.Users.GetByPredicateCount(
                  Predicates.Field<Users>(user => user.Email, Operator.Eq, model.Email));
            if (usersWithSuchEmail != 0)
            {
                Log.Error("Validation.ValidateUsersRegisterData - the email is not unique.");
                return ValidateHelper.EmailExist;
            }

            return null;
        }

        ///<inheritdoc/>
        public async Task<ErrorModel> ValidateUserProfile(int userId)
        {
            if (userId == 0)
            {
                Log.Error("Validation.ValidateUserProfile -  the user with id = 0.");
                return ValidateHelper.InvalidData;
            }

            var user = await _unitOfWork.Users.GetById(userId);
            if (user == null)
            {
                Log.Error(string.Concat("Validation.ValidateUserProfile -  the user with such id(Id - ", userId, ") was not found."));
                return ValidateHelper.InvalidData;
            }

            if (user.Deleted)
            {
                Log.Error("Validation.ValidateUserProfile -  the requested user is deleted.");
                return ValidateHelper.UserBlockedOrDeleted;
            }

            return null;
        }

        ///<inheritdoc/>
        public async Task<ErrorModel> IsSignIn(UserLogInDTO userDTO)
        {
            var foundedUsers = await _unitOfWork.Users.GetByPredicate(
                Predicates.Field<Users>(user => user.Email, Operator.Eq, userDTO.Email));

            var foundedUser = foundedUsers.FirstOrDefault();

            if (foundedUser == null)
            {
                Log.Error(string.Concat("Validation.IsSignIn -  the user with such login(Login - ", userDTO.Email, ") was not found."));
                return ValidateHelper.InvalidLoginOrPassword;
            }

            if (foundedUser.Deleted)
            {
                Log.Error(string.Concat("Validation.IsSignIn -  the requested user(Id - ", foundedUser.Id, ") is deleted."));
                return ValidateHelper.UserBlockedOrDeleted;
            }

            var hashFromModel = _passwordHasher.GetPasswordSaltHash(userDTO.Password, foundedUser.DynamicSalt);
            if (!hashFromModel.Equals(foundedUser.PasswordHash))
            {
                Log.Error(string.Concat("Validation.IsSignIn -  the user(Id - ", foundedUser.Id, ") enter invalid password."));
                return ValidateHelper.InvalidLoginOrPassword;
            }

            if (!foundedUser.EmailConfirmed)
            {
                Log.Error(string.Concat("Validation.IsSignIn -  the user(Id - ", foundedUser.Id, ") enter invalid password."));
                return ValidateHelper.EmailNotConfirmed;
            }

            return null;
        }

        ///<inheritdoc/>
        public async Task<ErrorModel> ValidateConfirmToken(int userId, string token)
        {
            if (userId == 0)
            {
                Log.Error("Validation.ValidateConfirmToken -  the user with id = 0.");
                return ValidateHelper.InvalidData;
            }

            var foundedUser = await _unitOfWork.Users.GetById(userId);
            if (foundedUser == null)
            {
                Log.Error("Validation.ValidateConfirmToken -  the user was not found.");
                return ValidateHelper.InvalidData;
            }

            if (!foundedUser.Token.Equals(token))
            {
                Log.Error(string.Concat("Validation.ValidateConfirmToken -  the user's token don't match(Id - ", foundedUser.Id, ")."));
                return ValidateHelper.InvalidData;
            }

            return null;
        }
    }
}
