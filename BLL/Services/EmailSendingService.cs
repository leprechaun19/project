﻿using MimeKit;
using MailKit.Net.Smtp;
using System.Threading.Tasks;
using BLL.Interfaces;
using System;
using Serilog;
using Common.Models;
using BLL.DTO;

namespace BLL.Services
{
    public class EmailSendingService : IEmailSendingService
    {
        private readonly SMTPSettings _smtpSettings;

        public EmailSendingService(SMTPSettings smtpServerSettings)
        {
            _smtpSettings = smtpServerSettings;
        }

        ///<inheritdoc/>
        public async Task SendEmailAsync(EmailRecieverModel model)
        {
            try
            {
                var emailMessage = GetHtmlMimeMessage(model);

                using var client = new SmtpClient();
                await client.ConnectAsync(_smtpSettings.HostName, _smtpSettings.Port, true);
                await client.AuthenticateAsync(_smtpSettings.SenderAddress, _smtpSettings.SenderPassword);
                await client.SendAsync(emailMessage);
                await client.DisconnectAsync(true);
            }
            catch (Exception e)
            {
                Log.Error(string.Format("{0}. EmailNotSend: email - {1}, the error message - {2}.", DateTime.UtcNow, model.RecieverAddress, e.Message));
            }
        }

        /// <summary>
        /// Fills only this fields: RecieverAddress, Message, Subject from the model with HTML body.
        /// </summary>
        /// <param name="model">The model with email settings which needs to send email.</param>
        /// <returns>The filled mime message.</returns>
        private MimeMessage GetHtmlMimeMessage(EmailRecieverModel model)
        {
            var emailMessage = new MimeMessage();
            emailMessage.From.Add(new MailboxAddress(_smtpSettings.SenderName, _smtpSettings.SenderAddress));
            emailMessage.To.Add(new MailboxAddress("", model.RecieverAddress));
            emailMessage.Subject = model.Subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html) { Text = model.Message };

            return emailMessage;
        }
    }
}
