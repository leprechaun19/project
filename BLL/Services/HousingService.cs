﻿using AutoMapper;
using BLL.DTO;
using BLL.Interfaces.IService;
using DAL.Entities;
using DAL.Entities.JoinEntities;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class HousingService : IHousingService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public HousingService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        /// <inheritdoc />
        public async Task<int> CreateAndGetId(HousingCreateDTO item)
        {
            Housing housing = _mapper.Map<HousingCreateDTO, Housing>(item);
            housing.CreationDate = DateTime.Today;
            var housingId = await _unitOfWork.Houses.CreateAndGetId(housing);
            _unitOfWork.SaveChanges();

            return housingId;
        }

        /// <inheritdoc />
        public async Task Delete(int housingId)
        {
            await _unitOfWork.Houses.Delete(housingId);
            await _unitOfWork.Comments.DeleteHousesComments(housingId);
            _unitOfWork.SaveChanges();
        }

        
        /// <inheritdoc />
        public async Task<HousingIndexDTO> Get(int id, int page)
        {
            Housing house = await _unitOfWork.Houses.GetById(id);
            Users user = await _unitOfWork.Users.GetById(house.UserId);
            IEnumerable<CommentWithUserName> comments = await _unitOfWork.Comments.GetCommentsWithUser(id, page);

            HousingIndexDTO housing = _mapper.Map<Housing, HousingIndexDTO>(house);
            housing.User = _mapper.Map<Users, UserInfoDTO>(user);
            housing.Comments = _mapper.Map<IEnumerable<CommentWithUserName>, IEnumerable<CommentHousingDTO>>(comments);

            return housing;
        }

        /// <inheritdoc />
        public async Task<IEnumerable<HousingInfoDTO>> SearchFilter(string Amount, int Adult, int Child, int SleepingPlaces, bool Bathroom,
            bool WiFi, bool BathAccessories, bool Television, bool Conditioner, bool Kitchen, bool Wardrobe, bool Microwave,
          bool WashingMachine, int page)
        {
            string[] cost = Amount.Replace(" ", "").Replace("$", "").Split('-');
            IEnumerable<Housing> houses = await _unitOfWork.Houses.SearchFilter(double.Parse(cost[0]), double.Parse(cost[1]), Adult,Child, SleepingPlaces, Bathroom,
            WiFi, BathAccessories, Television, Conditioner, Kitchen, Wardrobe, Microwave, WashingMachine, page);

            IEnumerable<HousingInfoDTO> housesDTO = _mapper.Map<IEnumerable<Housing>, IEnumerable<HousingInfoDTO>>(houses);

            return housesDTO;
        }

        /// <inheritdoc />
        public async Task<UserHousingDTO> GetUsersHousing(int housingId)
        {
            Housing house = await _unitOfWork.Houses.GetById(housingId);
            var housing = _mapper.Map<Housing, UserHousingDTO>(house);

            var schedules = await _unitOfWork.Bookings.GetBookingSchedule(housingId);
            housing.Schedules = _mapper.Map<IEnumerable<ScheduleOfBookingDTO>>(schedules);

            return housing;
        }

        /// <inheritdoc />
        public async Task<HousingUpdateDTO> GetForUpdate(int housingId)
        {
            Housing house = await _unitOfWork.Houses.GetById(housingId);
            HousingUpdateDTO housing = _mapper.Map<Housing, HousingUpdateDTO>(house);

            return housing;
        }

        /// <inheritdoc />
        public async Task HideAd(int id)
        {
            await _unitOfWork.Houses.HideAd(id);
            _unitOfWork.SaveChanges();
        }

        /// <inheritdoc />
        public async Task AddMainPhoto(int id, string mainImgUrl)
        {
            var house = await _unitOfWork.Houses.GetById(id);

            house.MainImgUrl = mainImgUrl;

            await _unitOfWork.Houses.Update(house);
            _unitOfWork.SaveChanges();
        }

        /// <inheritdoc />
        public async Task<IEnumerable<HousingInfoDTO>> Search(string headline, int page)
        {
            IEnumerable<Housing> houses = await _unitOfWork.Houses.SearchHouses(headline, page);
            IEnumerable<HousingInfoDTO> housesDTO = _mapper.Map<IEnumerable<Housing>, IEnumerable<HousingInfoDTO>>(houses);

            return housesDTO;
        }

        /// <inheritdoc />
        public async Task ShowAd(int id)
        {
            await _unitOfWork.Houses.ShowAd(id);
            _unitOfWork.SaveChanges();
        }

        /// <inheritdoc />
        public async Task Update(HousingUpdateDTO housing)
        {
            var house = await _unitOfWork.Houses.GetById(housing.Id);

            house.UpdateDate = DateTime.Now;
            house.TypeOfHousing = housing.TypeOfHousing;

            if (!string.IsNullOrEmpty(housing.MainImgUrl))
            {
                house.MainImgUrl = housing.MainImgUrl;
            }

            house.AdHeadline = housing.AdHeadline;
            house.Address = housing.Address;
            house.Bathroom = housing.Bathroom;
            house.NumberOfBeds = housing.NumberOfBeds;
            house.NumberOfPossibleGuests = housing.NumberOfPossibleGuests;
            house.PriceNight = housing.PriceNight;
            house.SleepingPlaces = housing.SleepingPlaces;

            house.WiFi = housing.WiFi;
            house.BathAccessories = housing.BathAccessories;
            house.Conditioner = housing.Conditioner;
            house.Kitchen = housing.Kitchen;
            house.Microwave = housing.Microwave;
            house.Wardrobe = housing.Wardrobe;
            house.WashingMachine = housing.WashingMachine;
            house.Television = housing.Television;

            await _unitOfWork.Houses.Update(house);
            _unitOfWork.SaveChanges();
        }
    }
}
