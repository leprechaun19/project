﻿using AutoMapper;
using BLL.DTO;
using BLL.Interfaces.IService;
using Common.Enums;
using DAL.Entities;
using DAL.Interfaces;
using System;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class BookingService : IBookingService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public BookingService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.mapper = mapper;
            this.unitOfWork = unitOfWork;
        }

        ///<inheritdoc/>
        public async Task<int> Create(BookingCreateDTO item)
        {
            Bookings booking = mapper.Map<BookingCreateDTO,Bookings>(item);

            booking.CreationDate = DateTime.Today;
            booking.Status = BookingStatus.PreBooked;

            int id  = await unitOfWork.Bookings.CreateAndGetId(booking);
            unitOfWork.SaveChanges();
            return id;
        }

        ///<inheritdoc/>
        public async Task Delete(int bookingId)
        {
            await unitOfWork.Bookings.Delete(bookingId);
            unitOfWork.SaveChanges();
        }

        ///<inheritdoc/>
        public async Task<BookingIndexDTO> GetById(int bookingId)
        {
            Bookings booking = await unitOfWork.Bookings.GetById(bookingId);
            Housing housing = await unitOfWork.Houses.GetById(booking.HousingId);
            Comments comment = await unitOfWork.Comments.Get(booking.HousingId, booking.UserId);

            BookingIndexDTO bookingDTO = mapper.Map<Bookings, BookingIndexDTO>(booking);
            bookingDTO.Housing = mapper.Map<Housing, HousingIndexDTO>(housing);
            bookingDTO.Comment = mapper.Map<Comments, CommentUpdateDTO>(comment);

            return bookingDTO;
        }

        ///<inheritdoc/>
        public async Task ChangeStatus(int bookingId, BookingStatus status)
        {
            int statusNum = (int)status;
            await unitOfWork.Bookings.ChangeBookingStatus(bookingId, statusNum);
            unitOfWork.SaveChanges();
        }
    }
}
