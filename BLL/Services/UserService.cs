﻿using AutoMapper;
using DapperExtensions;
using BLL.DTO;
using BLL.Interfaces.IService;
using Common.Interfaces;
using Common.Password;
using DAL.Entities;
using DAL.Entities.JoinEntities;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IPasswordManager _passwordHasher;
        private readonly IMapper _mapper;

        public UserService(IUnitOfWork unitOfWork, IMapper mapper, IPasswordManager passwordHasher)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _passwordHasher = passwordHasher;
        }

        ///<inheritdoc/>
        public async Task<int> CreateItem(UserCreateDTO item)
        {
            Users user = _mapper.Map<UserCreateDTO, Users>(item);

            user.DynamicSalt = Convert.ToBase64String(SaltGenerator.GetRandomSalt());
            user.PasswordHash = _passwordHasher.GetPasswordSaltHash(item.Password, user.DynamicSalt);
            user.CreationDate = DateTime.Today;

            int userId = await _unitOfWork.Users.CreateAndGetId(user);
            _unitOfWork.SaveChanges();

            return userId;
        }

        ///<inheritdoc/>
        public async Task Delete(int userId)
        {
            var user = await _unitOfWork.Users.GetById(userId);

            if(user != null)
            {
                await _unitOfWork.Users.Delete(user);
            }
            
            await _unitOfWork.Houses.DeleteUsersHouses(userId);
            await _unitOfWork.Comments.DeleteUsersComments(userId);
            await _unitOfWork.Bookings.DeleteUserBookings(userId);
            _unitOfWork.SaveChanges();
        }

        ///<inheritdoc/>
        public async Task<UserUpdateDTO> GetPersonalProfile(int id)
        {
            Users user = await _unitOfWork.Users.GetById(id);
            return _mapper.Map<UserUpdateDTO>(user);
        }

        ///<inheritdoc/>
        public async Task ConfirmEmail(int userId)
        {
            Users user = await _unitOfWork.Users.GetById(userId);
            user.EmailConfirmed = true;

            await _unitOfWork.Users.Update(user);
            _unitOfWork.SaveChanges();
        }

        ///<inheritdoc/>
        public async Task<UserProfileDTO> GetUserProfile(int id, int page)
        {
            Users user = await _unitOfWork.Users.GetById(id);
            IEnumerable<Housing> houses = await _unitOfWork.Houses.GetUsersHouses(id, page);

            IEnumerable<HousingInfoDTO> housesDTO = _mapper.Map<IEnumerable<Housing>, IEnumerable<HousingInfoDTO>>(houses);
            UserProfileDTO userDTO = _mapper.Map<UserProfileDTO>(user);
            userDTO.Houses = housesDTO;
            return userDTO;
        }

        ///<inheritdoc/>
        public async Task<UserPersonalProfileDTO> GetItemWithBookings(int id, int page)
        {
            IEnumerable<BookingWithHousingInfo>  bookings = await _unitOfWork.Bookings.GetBookingsWithHousing(id, page);
            IEnumerable<BookingInfoDTO> bookingsDTO = _mapper.Map<IEnumerable<BookingWithHousingInfo>, IEnumerable<BookingInfoDTO>>(bookings);

            return new UserPersonalProfileDTO(id, bookingsDTO);
        }

        ///<inheritdoc/>
        public async Task<UserPersonalProfileDTO> GetItemWithComments(int id, int page)
        {
            IEnumerable<CommentWithHousingInfo> comms = await _unitOfWork.Comments.GetCommentsWithHousing(id, page);
            IEnumerable<CommentProfileDTO> commsDTO = _mapper.Map<IEnumerable<CommentWithHousingInfo>, IEnumerable<CommentProfileDTO>>(comms);

            return new UserPersonalProfileDTO(id, commsDTO);
        }

        ///<inheritdoc/>
        public async Task<UserPersonalProfileDTO> GetItemWithFavourites(int id, int page)
        {
            IEnumerable<FavouritesWithHousingInfo>  favs = await _unitOfWork.Favourites.GetFavouritesWithHousing(id, page);
            IEnumerable<FavouriteIndexDTO> favsDTO = _mapper.Map<IEnumerable<FavouritesWithHousingInfo>, IEnumerable<FavouriteIndexDTO>>(favs);

            return new UserPersonalProfileDTO(id, favsDTO);
        }

        ///<inheritdoc/>
        public async Task<UserPersonalProfileDTO> GetItemWithHouses(int id, int page)
        {
            IEnumerable<Housing>  houses = await _unitOfWork.Houses.GetUsersHouses(id, page);
            IEnumerable<HousingInfoDTO> housesDTO = _mapper.Map<IEnumerable<Housing>, IEnumerable<HousingInfoDTO>>(houses);

            return new UserPersonalProfileDTO(id, housesDTO);
        }

        ///<inheritdoc/>
        public async Task UpdateItem(UserUpdateDTO user)
        {
            Users updateUser = _mapper.Map<UserUpdateDTO, Users>(user);

            await _unitOfWork.Users.Update(updateUser);
            _unitOfWork.SaveChanges();
        }

        ///<inheritdoc/>
        public async Task ChangePassword(ChangePasswordDTO item, int userId)
        {
            var user = await _unitOfWork.Users.GetById(userId);

            user.PasswordHash = _passwordHasher.GetPasswordSaltHash(item.NewPassword, user.DynamicSalt);

            await _unitOfWork.Users.ChangePassword(user);
            _unitOfWork.SaveChanges();
        }

        ///<inheritdoc/>
        public async Task<UserUpdateDTO> GetByLogin(string login)
        {
            UserUpdateDTO userDTO = default;

            var foundedUsers = await _unitOfWork.Users.GetByPredicate(
                Predicates.Field<Users>(user => user.Email, Operator.Eq, login));

            if(foundedUsers != null && foundedUsers.Any())
            {
                var firstUser = foundedUsers.FirstOrDefault();
                userDTO = _mapper.Map<UserUpdateDTO>(firstUser);
            }

            return userDTO;
        }
    }
}
