﻿using AutoMapper;
using BLL.DTO;
using BLL.Interfaces.IService;
using DAL.Entities;
using DAL.Entities.JoinEntities;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class CommentService : ICommentService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CommentService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        ///<inheritdoc/>
        public async Task Create(CommentCreateDTO item)
        {
            Comments comment = _mapper.Map<CommentCreateDTO, Comments>(item);
            comment.CreationDate = DateTime.Today;

            await _unitOfWork.Houses.AddEvaluation(item.HousingId, item.Evaluation);
            await _unitOfWork.Comments.Create(comment);
            _unitOfWork.SaveChanges();

        }

        ///<inheritdoc/>
        public async Task Delete(int commentId)
        {
            var comment = await _unitOfWork.Comments.GetById(commentId);
            comment.Deleted = true;

            await _unitOfWork.Comments.Update(comment);
            _unitOfWork.SaveChanges();
        }

        ///<inheritdoc/>
        public async Task<IEnumerable<CommentHousingDTO>> GetCommentsForIndex()
        {
            var comments = await _unitOfWork.Comments.GetCommentsForIndex();
            return _mapper.Map< IEnumerable<CommentWithUserName>, IEnumerable<CommentHousingDTO>>(comments);
        }

        ///<inheritdoc/>
        public async Task Update(CommentUpdateDTO newComment)
        {
            var oldComment = await _unitOfWork.Comments.GetById(newComment.Id);
            oldComment.Text = newComment.Comment;

            if(oldComment.Evaluation != newComment.Evaluation)
            {
                await _unitOfWork.Houses.ChangeEvaluation(newComment.HousingId, newComment.Evaluation, oldComment.Evaluation);
                oldComment.Evaluation = newComment.Evaluation;
            }

            await _unitOfWork.Comments.Update(oldComment);
            _unitOfWork.SaveChanges();
        }
    }
}
