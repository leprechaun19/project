﻿using Microsoft.Extensions.DependencyInjection;
using DAL.Interfaces;
using DAL.Repositories;

namespace BLL.Infastructure
{
    public static class AddDALDependency
    {
        public static IServiceCollection AddDALServices(this IServiceCollection services, string connectionString)
        {
            services.AddScoped<IUnitOfWork>(s => new UnitOfWork(connectionString));
            return services;
        }
    }
}
