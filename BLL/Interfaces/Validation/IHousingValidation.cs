﻿using Common.Error;
using System.Threading.Tasks;

namespace BLL.Interfaces.Validation
{
    public interface IHousingValidation
    {
        /// <summary>
        /// Checks whether the user is owner of the housing.
        /// </summary>
        /// <param name="userId">The user Id.</param>
        /// <param name="housingId">The housing Id.</param>
        /// <returns>Returned ErrorModel with message or null.</returns>
        Task<ErrorModel> ValidateHousingToDelete(int housingId, int userId);

        /// <summary>
        /// Checks whether the housing is deleted.
        /// </summary>
        /// <param name="housingId">The housing Id.</param>
        /// <returns>Returned ErrorModel with message or null.</returns>
        Task<ErrorModel> ValidateHousingToBooking(int housingId);
    }
}
