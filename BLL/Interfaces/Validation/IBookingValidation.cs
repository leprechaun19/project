﻿using BLL.DTO;
using Common.Error;
using System;
using System.Threading.Tasks;

namespace BLL.Interfaces.Validation
{
    public interface IBookingValidation
    {
        /// <summary>
        /// Checks that user's dates for booking is free.
        /// </summary>
        /// <param name="bookingId">The id of booking.</param>
        /// <returns>Returned ErrorModel with message or null.</returns>
        Task<ErrorModel> CheckDatesIsFree(int bookingId);

        /// <summary>
        /// Checks whether the booking is paid and the user is owner of the booking.
        /// </summary>
        /// <param name="userId">The user Id.</param>
        /// <param name="bookingId">The booking Id.</param>
        /// <returns>Returned ErrorModel with message or null.</returns>
        Task<ErrorModel> ValidateBookingToDelete(int bookingId, int userId);

        /// <summary>
        /// Checks that user's dates for booking is free.
        /// </summary>
        /// <param name="model">The model with booking data. </param>
        /// <param name="start">The start date of booking.</param>
        /// <param name="end">The end date of booking.</param>
        /// <returns>Returned ErrorModel with message or null.</returns>
        Task<ErrorModel> ValidateBookingToCreate(BookingCreateDTO model, DateTime start, DateTime end);
    }
}
