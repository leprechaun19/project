﻿using Common.Error;
using System.Threading.Tasks;

namespace BLL.Interfaces.Validation
{
    public interface ICommentValidation
    {
        /// <summary>
        /// Checks whether the booking is paid and ended.
        /// </summary>
        /// <param name="bookingId">The booking Id.</param>
        /// <param name="housingId">The id of the housing that user books.</param>
        /// <param name="userId">The id of the user that books.</param>
        /// <returns>Returned ErrorModel with message or null.</returns>
        Task<ErrorModel> ValidateCommentToAdd(int bookingId, int userId, int housingId);
    }
}
