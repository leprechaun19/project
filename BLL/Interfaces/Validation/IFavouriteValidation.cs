﻿using Common.Error;
using System.Threading.Tasks;

namespace BLL.Interfaces.Validation
{
    public interface IFavouriteValidation
    {
        /// <summary>
        /// Validates id of user and housing before creating.
        /// </summary>
        /// <param name="housingId">The id of the housing.</param>
        /// <param name="userId">The id of the user.</param>
        /// <returns>Returned ErrorModel with message or null.</returns>
        Task<ErrorModel> ValidateToCreate(int userId, int housingId);
    }
}
