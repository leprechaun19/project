﻿using BLL.DTO;
using Common.Error;
using System.Threading.Tasks;

namespace BLL.Interfaces.Validation
{
    public interface IUserValidation
    {
        /// <summary>
        /// Checks phone uniqueness and email uniqueness.
        /// </summary>
        /// <param name="model">The model that contains email and phone to check.</param>
        /// <returns>Returned ErrorModel with message or null.</returns>
        Task<ErrorModel> ValidateUsersRegisterData(UserCreateDTO model);

        /// <summary>
        /// Verifies user's old password.
        /// </summary>
        /// <param name="oldPassword">The user's old password.</param>
        /// <param name="userId">The id of the user.</param>
        /// <returns>Returned ErrorModel with message or null.</returns>
        Task<ErrorModel> ValidateOldPassword(string oldPassword, int userId);

        /// <summary>
        /// Checks whether the user is deleted.
        /// </summary>
        /// <param name="userId">The user Id.</param>
        /// <returns>Returned ErrorModel with message or null.</returns>
        Task<ErrorModel> ValidateUserProfile(int userId);

        /// <summary>
        /// Validates user's token to confirm email.
        /// </summary>
        /// <param name="userId">The user's Id.</param>
        /// <param name="token">The user's token.</param>
        /// <returns>Returned ErrorModel with message or null.</returns>
        Task<ErrorModel> ValidateConfirmToken(int userId, string token);

        /// <summary>
        /// Checks whether the user's password and login and email confirmed.
        /// </summary>
        /// <param name="user">The model of user.</param>
        /// <returns>Returned ErrorModel with message or null.</returns>
        Task<ErrorModel> IsSignIn(UserLogInDTO user);
    }
}
