﻿using BLL.DTO;
using System.Threading.Tasks;

namespace BLL.Interfaces.IService
{
    public interface IFavouriteService
    {
        /// <summary>
        /// Creates favourite in DB.
        /// </summary>
        /// <param name="favouriteCreateDTO">The favourite which needs to add.</param>
        Task Create(FavouriteCreateDTO favouriteCreateDTO);

        /// <summary>
        /// Checks whether the user have it house in the favourite.
        /// </summary>
        /// <param name="userId">The id of user.</param>
        /// <param name="housingId">The id of housing.</param>
        /// <returns>The result of check.</returns>
        Task<bool> IsFavourite(int userId, int housingId);

        /// <summary>
        /// Deletes favourite in DB.
        /// </summary>
        /// <param name="favouriteId">The id of favourite.</param>
        Task Delete(int favouriteId);
    }
}
