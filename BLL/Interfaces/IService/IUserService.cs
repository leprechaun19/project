﻿using BLL.DTO;
using System.Threading.Tasks;

namespace BLL.Interfaces.IService
{
    public interface IUserService
    {
        /// <summary>
        /// Gets user profile by user Id.
        /// </summary>
        /// <param name="userId">The ID of the User.</param>
        /// <returns>The user profile with his Houses.</returns>
        Task<UserProfileDTO> GetUserProfile(int userId, int page);

        /// <summary>
        /// Gets personal profile by user Id.
        /// </summary>
        /// <param name="userId">The ID of the User.</param>
        /// <returns>The personal profile with his Houses.</returns>
        Task<UserUpdateDTO> GetPersonalProfile(int userId);

        /// <summary>
        /// Creates user item in DB.
        /// </summary>
        /// <param name="item">The DTO object which needs to added to the database.</param>
        /// <returns>The id of added user.</returns>
        Task<int> CreateItem(UserCreateDTO item);

        /// <summary>
        /// Changes bool value EmailConfirmed on true.
        /// </summary>
        /// <param name="userId">The ID of the User.</param>
        Task ConfirmEmail(int userId);

        /// <summary>
        /// Updates user item.
        /// </summary>
        /// <param name="user">The user item which needs to updated.</param>
        Task UpdateItem(UserUpdateDTO user);

        /// <summary>
        /// Finds user by his login(email).
        /// </summary>
        /// <param name="login">The login of the User.</param>
        /// <returns>The model of user.</returns>
        Task<UserUpdateDTO> GetByLogin(string login);

        /// <summary>
        /// Changes User's password hash.
        /// </summary>
        /// <param name="item">The item of User with new password hash.</param>
        Task ChangePassword(ChangePasswordDTO item, int userId);

        /// <summary>
        /// Gets user with his Favourites.
        /// </summary>
        /// <param name="id">The ID of the User.</param>
        /// <param name="page">The page number which needs to got 20 Favourites.</param>
        /// <returns>The User with his Favourites.</returns>
        Task<UserPersonalProfileDTO> GetItemWithFavourites(int id, int page);

        /// <summary>
        /// Gets user with his Houses.
        /// </summary>
        /// <param name="id">The ID of the User.</param>
        /// <param name="page">The page number which needs to got 20 Houses.</param>
        /// <returns>The user with his houses.</returns>
        Task<UserPersonalProfileDTO> GetItemWithHouses(int id, int page);

        /// <summary>
        /// Gets user with his comments.
        /// </summary>
        /// <param name="id">The ID of the User.</param>
        /// <param name="page">The page number which needs to got 20 Comments.</param>
        /// <returns>The user with his comments.</returns>
        Task<UserPersonalProfileDTO> GetItemWithComments(int id, int page);

        /// <summary>
        /// Gets user with his bookings.
        /// </summary>
        /// <param name="id">The ID of the User.</param>
        /// <param name="page">The page number which needs to got 20 Bookings.</param>
        /// <returns>The user with his bookings.</returns>
        Task<UserPersonalProfileDTO> GetItemWithBookings(int id, int page);

        /// <summary>
        /// Sets Delete field to true and deletes houses, comment and bookings.
        /// </summary>
        /// <param name="userId">The user id.</param>
        Task Delete(int userId);
    }
}
