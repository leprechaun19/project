﻿using BLL.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces.IService
{
    public interface IHousingService
    {
        /// <summary>
        /// Search by parameters.
        /// </summary>
        /// <returns>Returned the founded houses.</returns>
        Task<IEnumerable<HousingInfoDTO>> SearchFilter(string Amount, int Adult, int Child, int SleepingPlaces, bool Bathroom,
            bool WiFi, bool BathAccessories, bool Television, bool Conditioner, bool Kitchen, bool Wardrobe, bool Microwave,
          bool WashingMachine, int page);
        
        /// <summary>
        /// Updates housing.
        /// </summary>
        /// <param name="housing">The model with new data.</param>
        Task Update(HousingUpdateDTO housing);

        /// <summary>
        /// Gets housing by id with comments by page.
        /// </summary>
        /// <param name="housingId">The id of housing.</param>
        /// <param name="commentsPage">The page of comments.</param>
        /// <returns>The found housing with comments.</returns>
        Task<HousingIndexDTO> Get(int housingId, int commentsPage);

        /// <summary>
        /// Gets housing by id.
        /// </summary>
        /// <param name="housingId">The id of housing.</param>
        /// <returns>The found housing.</returns>
        Task<HousingUpdateDTO> GetForUpdate(int housingId);

        /// <summary>
        /// Gets user's housing with booking schedule.
        /// </summary>
        /// <param name="housingId">The id of housing.</param>
        /// <returns>The found  with booking schedule.</returns>
        Task<UserHousingDTO> GetUsersHousing(int housingId);

        /// <summary>
        /// Searches housings by headline on specific page.
        /// </summary>
        /// <param name="headline">The search headline.</param>
        /// <param name="page">The page.</param>
        /// <returns>The found housings.</returns>
        Task<IEnumerable<HousingInfoDTO>> Search(string headline, int page);

        /// <summary>
        /// Sets Deleted field to true and deletes comments.
        /// </summary>
        /// <param name="housingId">The id of housing.</param>
        Task Delete(int housingId);

        /// <summary>
        /// Maps Housing Create DTO to Housing entity and adds item to the database.
        /// </summary>
        /// <param name="item">The DTO object which needs to added to the database.</param>
        Task<int> CreateAndGetId(HousingCreateDTO item);

        /// <summary>
        /// Added string path of image to housing in db.
        /// </summary>
        /// <param name="housingId">The ID of the housing.</param>
        /// <param name="mainImgPath">The path to image.</param>
        Task AddMainPhoto(int housingId, string mainImgPath);

        /// <summary>
        /// Change Hidden field to true in the database.
        /// </summary>
        /// <param name="housingId">The ID of the housing.</param>
        Task HideAd(int housingId);

        /// <summary>
        /// Change Hidden field to false in the database.
        /// </summary>
        /// <param name="housingId">The ID of the housing.</param>
        Task ShowAd(int housingId);
    }
}
