﻿using BLL.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces.IService
{
    public interface ICommentService 
    {
        /// <summary>
        /// Gets last 4 comments for Home Index page.
        /// </summary>
        /// <returns>The list of comments.</returns>
        Task<IEnumerable<CommentHousingDTO>> GetCommentsForIndex();

        /// <summary>
        /// Creates comment item in DB.
        /// </summary>
        /// <param name="commentCreateDTO">The DTO object which needs to be added to the database.</param>
        Task Create(CommentCreateDTO commentCreateDTO);

        /// <summary>
        /// Sets Deleted field to true.
        /// </summary>
        /// <param name="commentId">The comment id.</param>
        Task Delete(int commentId);

        /// <summary>
        /// Updates some fields in comment entity and housing evaluation.
        /// </summary>
        /// <param name="comment">The comment item which needs to updates.</param>
        Task Update(CommentUpdateDTO comment);

    }
}
