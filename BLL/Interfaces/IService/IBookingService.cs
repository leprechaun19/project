﻿using BLL.DTO;
using Common.Enums;
using System.Threading.Tasks;

namespace BLL.Interfaces.IService
{
    public interface IBookingService
    {
        /// <summary>
        /// Gets the booking by his Id.
        /// </summary>
        /// <param name="bookingId">The ID of the Booking.</param>
        /// <returns>The found booking.</returns>
        Task<BookingIndexDTO> GetById(int bookingId);

        /// <summary>
        /// Adds booking to the database.
        /// </summary>
        /// <param name="bookingCreateDTO">The booking which needs to add.</param>
        /// <returns>The id of added booking.</returns>
        Task<int> Create(BookingCreateDTO bookingCreateDTO);

        /// <summary>
        /// Sets Deleted field for booking to true.
        /// </summary>
        /// <param name="bookingId">The id of booking.</param>
        Task Delete(int bookingId);

        /// <summary>
        /// Changes the status of the booking.
        /// </summary>
        /// <param name="bookingId">The booking Id.</param>
        /// <param name="status">The new status.</param>
        Task ChangeStatus(int bookingId, BookingStatus status);
    }
}
