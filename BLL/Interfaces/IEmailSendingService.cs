﻿using BLL.DTO;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IEmailSendingService
    {
        /// <summary>
        /// Sends email message to the reciever.
        /// </summary>
        /// <param name="model">The model with email reciever settings.</param>
        Task SendEmailAsync(EmailRecieverModel model);
    }
}
