﻿
namespace BLL.DTO
{
    public class EmailRecieverModel
    {
        public string RecieverAddress { get; set; }
        public string Message { get; set; }
        public string Subject { get; set; }
    }
}
