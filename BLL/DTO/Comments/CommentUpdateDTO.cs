﻿
namespace BLL.DTO
{
    public class CommentUpdateDTO 
    {
        public int Id { get; set; }
        public int HousingId { get; set; }
        public int UserId { get; set; }

        public short Evaluation { get; set; }
        public string Comment { get; set; }

    }
}
