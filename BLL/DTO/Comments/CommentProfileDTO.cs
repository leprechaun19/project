﻿using System;

namespace BLL.DTO
{
    public class CommentProfileDTO
    {
        public int Id { get; set; }
        public HousingInfoDTO Housing { get; set; }

        public DateTime CreationDate { get; set; }
        public short Evaluation { get; set; }
        public string Comment { get; set; }
    }
}
