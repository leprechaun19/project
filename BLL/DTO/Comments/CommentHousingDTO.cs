﻿using System;

namespace BLL.DTO
{
    public class CommentHousingDTO 
    {
        public int UserId { get; set; }
        public string UserName { get; set; }

        public DateTime CreationDate { get; set; }

        public short Evaluation { get; set; }
        public string Comment { get; set; }
    }
}
