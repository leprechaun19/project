﻿
namespace BLL.DTO
{
    public class CommentCreateDTO 
    {
        public int HousingId { get; set; }
        public int UserId { get; set; }

        public short Evaluation { get; set; }
        public string Comment { get; set; }

        public bool Deleted { get; set; }
    }
}
