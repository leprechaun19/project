﻿using Common.Enums;
using System;

namespace BLL.DTO
{
    public class HousingInfoDTO
    { 
        public int Id { get; set; }
        public UserInfoDTO User { get; set; }

        public TypeOfHousing TypeOfHousing { get; set; }
        public string AdHeadline { get; set; }
        public string MainImgUrl { get; set; }
        public string Address { get; set; }
        public double PriceNight { get; set; }

        public int CommentAmount { get; set; }
        public float AverageEvaluation { get; set; }

        public DateTime CreationDate { get; set; }
    }
}
