﻿using Common.Enums;
using System;
using System.Collections.Generic;

namespace BLL.DTO
{
    public class HousingIndexDTO
    {
        public int Id { get; set; }
        public TypeOfHousing TypeOfHousing { get; set; }
        public string AdHeadline { get; set; }
        public string Address { get; set; }
        public string MainImgUrl { get; set; }
        public double PriceNight { get; set; }

        public float AverageEvaluation { get; set; }

        public int NumberOfPossibleGuests { get; set; }
        public int NumberOfBeds { get; set; }
        public int SleepingPlaces { get; set; }
        public bool Bathroom { get; set; }
        public DateTime CreationDate { get; set; }

        public bool WiFi { get; set; }
        public bool BathAccessories { get; set; }
        public bool Television { get; set; }
        public bool Conditioner { get; set; }
        public bool Kitchen { get; set; }
        public bool Wardrobe { get; set; }
        public bool Microwave { get; set; }
        public bool WashingMachine { get; set; }

        public UserInfoDTO User { get; set; }
        public IEnumerable<CommentHousingDTO> Comments { get; set; }
    }
}
