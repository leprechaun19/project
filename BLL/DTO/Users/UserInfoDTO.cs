﻿

namespace BLL.DTO
{
    public class UserInfoDTO
    {
        public int Id { get; set; }
        public string UserName { get; set; }
    }
}
