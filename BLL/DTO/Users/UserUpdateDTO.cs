﻿using System;

namespace BLL.DTO
{
    public class UserUpdateDTO 
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public DateTime? DOB { get; set; }

        public string Token { get; set; }

        public string AboutMe { get; set; }

        public bool EmailConfirmed { get; set; }
        public bool PhoneConfirmed { get; set; }
        public bool IsAdmin { get; set; }
    }
}
