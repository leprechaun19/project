﻿using System.Collections.Generic;

namespace BLL.DTO
{
    public class UserPersonalProfileDTO
    {
        public int Id { get; }

        public IEnumerable<FavouriteIndexDTO> Favourites { get;  }
        public IEnumerable<BookingInfoDTO> Bookings { get; }
        public IEnumerable<CommentProfileDTO> Comments { get; }
        public IEnumerable<HousingInfoDTO> Houses { get; }

        public UserPersonalProfileDTO(int id, IEnumerable<FavouriteIndexDTO> favourites)
        {
            Id = id;
            Favourites = favourites;

        }
        public UserPersonalProfileDTO(int id, IEnumerable<BookingInfoDTO> bookings)
        {
            Id = id;
            Bookings = bookings;

        }
        public UserPersonalProfileDTO(int id, IEnumerable<CommentProfileDTO> comments)
        {
            Id = id;
            Comments = comments;

        }
        public UserPersonalProfileDTO(int id, IEnumerable<HousingInfoDTO> houses)
        {
            Id = id;
            Houses = houses;

        }
    }
}
