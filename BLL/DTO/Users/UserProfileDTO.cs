﻿using System.Collections.Generic;

namespace BLL.DTO
{
    public class UserProfileDTO
    {
        public int Id { get; set; }
        public string UserName { get; set; }

        public string AboutMe { get; set; }
        public bool EmailConfirmed { get; set; }
        public bool PhoneConfirmed { get; set; }

        public IEnumerable<HousingInfoDTO> Houses { get; set; }
    }
}
