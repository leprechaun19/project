﻿using Common.Enums;

namespace BLL.DTO
{
    public class UserCreateDTO 
    {   
        public UserCreateDTO()
        {
            Language = Languages.ru;
        }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Phone { get; set; }
        public string PasswordHash { get; set; }
        public string Password { get; set; }
        public string DynamicSalt { get; set; }

        public string Token { get; set; }

        public Languages Language { get; set; }
        public bool EmailConfirmed { get; set; }
        public bool PhoneConfirmed { get; set; }
    }
}
