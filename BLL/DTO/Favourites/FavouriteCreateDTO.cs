﻿

namespace BLL.DTO
{
    public class FavouriteCreateDTO 
    {
        public int UserId { get; set; }
        public int HousingId { get; set; }
    }
}
