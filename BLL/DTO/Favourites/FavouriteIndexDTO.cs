﻿using System;

namespace BLL.DTO
{
    public class FavouriteIndexDTO 
    {
        public int Id { get; set; }
        public HousingInfoDTO Housing { get; set; }
            
        public DateTime CreationDate { get; set; }
    }
}
