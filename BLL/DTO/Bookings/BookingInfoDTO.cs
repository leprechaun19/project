﻿using Common.Enums;
using System;

namespace BLL.DTO
{
    public class BookingInfoDTO
    {
        public int Id { get; set; }
        public HousingInfoDTO Housing { get; set; }

        public BookingStatus Status { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CreationDate { get; set; }

        public double Cost { get; set; }

        public int Adults { get; set; }
        public int ChildrenFrom6To17 { get; set; }
    }
}
