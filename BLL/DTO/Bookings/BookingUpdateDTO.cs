﻿using Common.Enums;

namespace BLL.DTO
{
    public class BookingUpdateDTO 
    {
        public int Id { get; set; }
        public BookingStatus Status { get; set; }
    }
}
