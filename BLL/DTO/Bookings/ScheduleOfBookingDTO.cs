﻿using System;

namespace BLL.DTO
{
    public class ScheduleOfBookingDTO
    {
        public string UserName { get; set; }
        public int UserId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
