﻿using Common.Enums;
using System;

namespace BLL.DTO
{
    public class BookingCreateDTO 
    {
        public BookingCreateDTO()
        {
            Status = BookingStatus.NotBooked;
        }

        public int UserId { get; set; }
        public int HousingId { get; set; }

        public BookingStatus Status { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public double Cost { get; set; }

        public int Adults { get; set; }
        public int ChildrenFrom6To17 { get; set; }

        public bool Deleted { get; set; }
    }
}
