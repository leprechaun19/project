﻿using Dapper;
using DAL.Entities;
using DAL.Entities.JoinEntities;
using DAL.Interfaces.Repositories;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    internal class FavouriteRepository : BaseRepository<Favourites>, IFavouritesRepository
    {
        public FavouriteRepository(IDbTransaction transaction) : base(transaction)
        {
        }

        ///<inheritdoc/>
        public async Task<IEnumerable<FavouritesWithHousingInfo>> GetFavouritesWithHousing(int userId, int page)
        {
            int to = page * 20;
            int from = to - 20;
            var selectQuery = new StringBuilder();

            selectQuery.Append(" SELECT f.Id, f.CreationDate, h.Id, h.AdHeadline, h.Address, h.Pricenight, h.AverageEvaluation, h.CommentAmount, h.TypeOfHousing, h.MainImgUrl  ");
            selectQuery.Append(" FROM Favourites as f INNER JOIN Housing as h ON f.HousingId = h.Id ");
            selectQuery.Append(" WHERE f.UserId = @userId ORDER BY f.Id desc OFFSET @from ROWS FETCH NEXT @to ROWS ONLY ");

            var favourites = await Connection.QueryAsync<Favourites, Housing, FavouritesWithHousingInfo>(selectQuery.ToString(),
                (favourite, housing) => {
                    return new FavouritesWithHousingInfo
                    {
                        Address = housing.Address,
                        AdHeadline = housing.AdHeadline,
                        Id = favourite.Id,
                        HousingId = housing.Id,
                        PriceNight = housing.PriceNight,
                        AverageEvaluation = housing.AverageEvaluation,
                        CreationDate = favourite.CreationDate,
                        CommentAmount = housing.CommentAmount,
                        TypeOfHousing = housing.TypeOfHousing,
                        MainImgUrl = housing.MainImgUrl
                    };

                }, new { userId, from, to }, transaction: Transaction);

            return favourites;
        }
    }
}
