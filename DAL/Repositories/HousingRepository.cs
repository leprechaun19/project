﻿using Dapper;
using DAL.Entities;
using DAL.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    internal class HousingRepository : BaseRepository<Housing>, IHousingRepository
    {
        public HousingRepository(IDbTransaction transaction) : base(transaction)
        {
        }

        ///<inheritdoc/>
        public async Task<int> CreateAndGetId(Housing housing)
        {
            var createQuery = new StringBuilder();
            createQuery.Append(" INSERT INTO Housing ( UserId, TypeOfHousing, AdHeadline, Address, PriceNight, CreationDate, UpdateDate, NumberOfPossibleGuests, NumberOfBeds, SleepingPlaces, Bathroom, GeoLatitude, GeoLongitude, Deleted, Hidden)");
            createQuery.Append(" VALUES( @UserId, @TypeOfHousing, @AdHeadline, @Address, @PriceNight, @CreationDate, @UpdateDate, @NumberOfPossibleGuests, @NumberOfBeds, @SleepingPlaces, @Bathroom, @GeoLatitude, @GeoLongitude, @Deleted, @Hidden); ");
            createQuery.Append(" SELECT CAST(SCOPE_IDENTITY() as int); ");

            var housingId = await Connection.ExecuteScalarAsync<int>(createQuery.ToString(), housing, transaction: Transaction);

            return housingId;
        }

        ///<inheritdoc/>
        public async Task Delete(int housingId)
        {
            var deleteQuery = " UPDATE Housing SET Deleted = 1 WHERE Id = @id ";
            await Connection.ExecuteAsync(deleteQuery, new { housingId }, transaction: Transaction);
        }

        ///<inheritdoc/>
        public async Task DeleteUsersHouses(int userId)
        {
            var deleteQuery = new StringBuilder();
            deleteQuery.Append(" UPDATE Housing SET Deleted = 1 WHERE UserId = @userId; ");

            await Connection.ExecuteAsync(deleteQuery.ToString(), userId, transaction: Transaction);
        }

        ///<inheritdoc/>
        public async Task HideAd(int housingId)
        {
            var deleteQuery = " UPDATE Housing SET Hidden = 1 WHERE Id = @id ";
            await Connection.ExecuteAsync(deleteQuery, housingId, transaction: Transaction);
        }

        ///<inheritdoc/>
        public async Task ShowAd(int housingId)
        {
            var deleteQuery = " UPDATE Housing SET Hidden = 0 WHERE Id = @id ";
            await Connection.ExecuteAsync(deleteQuery, housingId, transaction: Transaction);
        }

        ///<inheritdoc/>
        public async Task<IEnumerable<Housing>> GetUsersHouses(int userId, int page)
        {
            int to = page * 20;
            int from = to - 20;
            var selectQuery = new StringBuilder();

            selectQuery.Append(" SELECT * FROM Housing as h ");
            selectQuery.Append(" WHERE h.UserId = @userId AND Deleted = 0 ");
            selectQuery.Append(" ORDER BY h.Id desc OFFSET @from ROWS FETCH NEXT @to ROWS ONLY ");

            var usersHouses = await Connection.QueryAsync<Housing>(selectQuery.ToString(),
                new { userId, from, to }, transaction: Transaction);
            return usersHouses;
        }

        ///<inheritdoc/>
        public async Task<IEnumerable<Housing>> SearchHouses(string housingHeadline, int page)
        {
            int to = page * 20;
            int from = to - 20;
            var query = new StringBuilder();

            query.Append(" SELECT * FROM Housing as h WHERE h.AdHeadline Like  CONCAT('%',@housingHeadline,'%') AND Deleted = 0 ");
            query.Append(" ORDER BY h.Id desc OFFSET @from ROWS FETCH NEXT @to ROWS ONLY ");

            var searchHouses = await Connection.QueryAsync<Housing>(query.ToString(), new { housingHeadline, from, to }, transaction: Transaction);
            return searchHouses;
        }

        ///<inheritdoc/>
        public async Task<bool> IsDatesFree(int housingId, DateTime start, DateTime end)
        {
            var sql = new StringBuilder();
            sql.Append(" SELECT * FROM Housing as h INNER JOIN Bookings as b ");
            sql.Append(" ON h.Id = b.HousingId ");
            sql.Append(" WHERE h.Id = @housingId AND b.Deleted = 0 AND  b.Status = 3 AND ");
            sql.Append(" ( @start between b.StartDate and b.EndDate ");
            sql.Append(" OR @end between b.StartDate and b.EndDate ");
            sql.Append(" OR b.StartDate between @start and @end) ");

            var housings = await Connection.QueryAsync<Housing, Bookings, Housing>(sql.ToString(),
                map: (housing, booking) => {
                    return housing;

                }, splitOn: "HousingId", param: new { housingId, start, end }, transaction: Transaction);

            return !housings.Any();
        }

        ///<inheritdoc/>
        public async Task<IEnumerable<Housing>> SearchFilter(double costFrom, double costTo, int Adult, int Child, int SleepingPlaces, bool Bathroom,
            bool WiFi, bool BathAccessories, bool Television, bool Conditioner, bool Kitchen, bool Wardrobe, bool Microwave,
          bool WashingMachine, int page)
        {
            int to = page * 20;
            int from = to - 20;
            int possibleGuests = Adult + Child;

            var selectQuery = new StringBuilder();
            selectQuery.Append(" SELECT * FROM Housing as h ");
            selectQuery.Append(" inner join Conveniences as c ");
            selectQuery.Append(" on h.Id = c.Id ");
            selectQuery.Append(" WHERE h.Deleted = 0 and h.Hidden = 0 and h.NumberOfPossibleGuests >= @possibleGuests and ");
            selectQuery.Append(" h.SleepingPlaces >= @SleepingPlaces and h.PriceNight between @costFrom and @costTo ");

            if (Bathroom)
            {
                selectQuery.Append(" and h.Bathroom = 1 ");
            }
            if (WiFi)
            {
                selectQuery.Append(" and c.WiFi = 1 ");
            }
            if (BathAccessories)
            {
                selectQuery.Append(" and c.BathAccessories = 1 ");
            }
            if (Television)
            {
                selectQuery.Append(" and c.Television = 1 ");
            }
            if (Conditioner)
            {
                selectQuery.Append(" and c.Conditioner = 1 ");
            }
            if (Kitchen)
            {
                selectQuery.Append(" and c.Kitchen = 1 ");
            }
            if (Wardrobe)
            {
                selectQuery.Append(" and c.Wardrobe = 1 ");
            }
            if (Microwave)
            {
                selectQuery.Append(" and c.Microwave = 1 ");
            }
            if (WashingMachine)
            {
                selectQuery.Append(" and c.WashingMachine = 1 ");
            }
            selectQuery.Append(" ORDER BY h.Id desc OFFSET @from ROWS FETCH NEXT @to ROWS ONLY ");

            var foundHouses = await Connection.QueryAsync<Housing>(selectQuery.ToString(),
                new { from, to, possibleGuests , SleepingPlaces, costFrom, costTo }, transaction: Transaction);

            return foundHouses;
        }

        ///<inheritdoc/>
        public async Task AddEvaluation(int housingId, int evaluation)
        {
            var housing = await GetById(housingId);

            housing.SumEvaluation += evaluation;
            housing.CommentAmount += 1;
            housing.AverageEvaluation = (float)housing.SumEvaluation/ (float)housing.CommentAmount;

            var updateQuery = new StringBuilder();
            updateQuery.Append(" UPDATE Housing SET AverageEvaluation = @AverageEvaluation, SumEvaluation = @SumEvaluation, ");
            updateQuery.Append(" CommentAmount = @CommentAmount ");
            updateQuery.Append(" WHERE Id = @Id");

            await Connection.ExecuteAsync(updateQuery.ToString(), housing, transaction: Transaction);
        }

        ///<inheritdoc/>
        public async Task ChangeEvaluation(int housingId, int newEvaluation, int oldEvaluation)
        {
            var housing = await GetById(housingId); 
            
            housing.SumEvaluation = housing.SumEvaluation - oldEvaluation + newEvaluation;
            housing.AverageEvaluation = (float)housing.SumEvaluation / (float)housing.CommentAmount;

            var updateQuery = new StringBuilder();
            updateQuery.Append(" UPDATE Housing SET AverageEvaluation = @AverageEvaluation, SumEvaluation = @SumEvaluation ");
            updateQuery.Append(" WHERE Id = @Id");

            await Connection.ExecuteAsync(updateQuery.ToString(), housing, transaction: Transaction);
        }
    }
}
