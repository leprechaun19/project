﻿using DAL.Interfaces.Repositories;
using DAL.Entities;
using System.Data;
using Dapper;
using System.Threading.Tasks;
using System.Text;

namespace DAL.Repositories
{
    internal class UserRepository : BaseRepository<Users>, IUserRepository
    {
        public UserRepository(IDbTransaction transaction) : base(transaction)
        {
        }

        ///<inheritdoc />
        public async Task<int> CreateAndGetId(Users user)
        {
            var createQuery = new StringBuilder();
            createQuery.Append(" INSERT INTO Users ( UserName, DOB, Email, Address, Phone, PasswordHash, Language, DynamicSalt, AboutMe, CreationDate, EmailConfirmed, PhoneConfirmed, Deleted, Token)");
            createQuery.Append(" VALUES( @UserName, @DOB, @Email, @Address, @Phone, @PasswordHash, @Language, @DynamicSalt, @AboutMe, @CreationDate, @EmailConfirmed, @PhoneConfirmed, @Deleted, @Token); ");
            createQuery.Append(" SELECT CAST(SCOPE_IDENTITY() as int); ");

            var userId = await Connection.ExecuteScalarAsync<int>(createQuery.ToString(), user, transaction: Transaction);
            return userId;
        }

        ///<inheritdoc cref="BaseRepository{T}"/>
        public new async Task Update(Users user)
        {
            var updateQuery = new StringBuilder();
            updateQuery.Append(" UPDATE Users SET UserName = @UserName, DOB = @DOB, Email = @Email,");
            updateQuery.Append(" Address = @Address, Phone = @Phone, AboutMe = @AboutMe, ");
            updateQuery.Append(" EmailConfirmed = @EmailConfirmed, PhoneConfirmed = @PhoneConfirmed ");
            updateQuery.Append(" WHERE Id = @Id ");

            await Connection.ExecuteAsync(updateQuery.ToString(), user, transaction: Transaction);
        }

        ///<inheritdoc />
        public async Task ChangePassword(Users item)
        {
            var changeHash = " UPDATE Users SET PasswordHash = @PasswordHash WHERE Id = @Id ";
            await Connection.ExecuteAsync(changeHash, item, transaction: Transaction);
        }
    }
}
