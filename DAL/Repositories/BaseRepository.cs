﻿using DapperExtensions;
using DAL.Interfaces.Repositories;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    /// <summary>
    /// Generalized class provides common methods for accessing data. 
    /// </summary>
    /// <typeparam name="T">The repository for concrete db entity.</typeparam>
    abstract class BaseRepository<T> : IRepository<T> where T : class
    {
        protected IDbTransaction Transaction { get; private set; }
        protected IDbConnection Connection => Transaction.Connection;


        public BaseRepository(IDbTransaction transaction)
        {
            Transaction = transaction;
        }

        /// <inheritdoc/>
        public virtual async Task Create(T item)
        {
            await Connection.InsertAsync<T>(item, transaction: Transaction);
        }

        /// <inheritdoc/>
        public virtual async Task Delete(T item)
        {
            await Connection.DeleteAsync<T>(item, transaction: Transaction);
        }

        /// <inheritdoc/>
        public virtual async Task<T> GetById(int id)
        {
            return await Connection.GetAsync<T>(id, transaction: Transaction);
        }

        /// <inheritdoc/>
        public virtual async Task<int> GetByPredicateGroupCount(IPredicateGroup predicateGroup)
        {
            return await Connection.CountAsync<T>(predicateGroup, transaction: Transaction);
        }

        /// <inheritdoc/>
        public virtual async Task<int> GetByPredicateCount(IFieldPredicate predicate)
        {
            return await Connection.CountAsync<T>(predicate, transaction: Transaction);
        }

        /// <inheritdoc/>
        public virtual async Task<IEnumerable<T>> GetByPredicate(IFieldPredicate predicate)
        {
            return await Connection.GetListAsync<T>(predicate, transaction: Transaction);
        }

        /// <inheritdoc/>
        public virtual async Task Update(T item)
        {
            await Connection.UpdateAsync(item, transaction: Transaction);
        }

        /// <inheritdoc/>
        public virtual async Task<IEnumerable<T>> GetByPredicateGroup(IPredicateGroup predicate)
        {
            return await Connection.GetListAsync<T>(predicate, transaction: Transaction);
        }

        /// <inheritdoc/>
        public virtual async Task<IEnumerable<T>> GetPageByPredicateGroup(IPredicateGroup predicate, int page)
        {
            return await Connection.GetPageAsync<T>(predicate, page: page, transaction: Transaction);
        }
    }
}