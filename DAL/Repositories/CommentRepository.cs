﻿using Dapper;
using DAL.Entities;
using DAL.Entities.JoinEntities;
using DAL.Interfaces.Repositories;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DapperExtensions;

namespace DAL.Repositories
{
    internal class CommentRepository : BaseRepository<Comments>, ICommentRepository
    {
        public CommentRepository(IDbTransaction transaction) : base(transaction)
        { 
        }

        ///<inheritdoc/>
        public async Task<Comments> Get(int housingId, int userId)
        {
            PredicateGroup predicateGroup = new() { Operator = GroupOperator.And, Predicates = new List<IPredicate>() };
            predicateGroup.Predicates.Add(Predicates.Field<Comments>(comment => comment.UserId, Operator.Eq, userId));
            predicateGroup.Predicates.Add(Predicates.Field<Comments>(comment => comment.HousingId, Operator.Eq, housingId));

            var comments = await GetByPredicateGroup(predicateGroup);

            return comments.FirstOrDefault();
        }

        ///<inheritdoc/>
        public async Task DeleteUsersComments(int userId)
        {
            var deleteQuery = " UPDATE Comments SET Deleted = 1 WHERE UserId = @userId ";
            await Connection.ExecuteAsync(deleteQuery, userId, transaction: Transaction);
        }

        ///<inheritdoc/>
        public async Task DeleteHousesComments(int HousingId)
        {
            var deleteQuery = " UPDATE Comments SET Deleted = 1 WHERE HousingId = @housingId ";
            await Connection.ExecuteAsync(deleteQuery, new { housingId = HousingId }, transaction: Transaction);
        }

        ///<inheritdoc/>
        public async Task<IEnumerable<CommentWithUserName>> GetCommentsForIndex()
        {
            int goodEvaluation = 4;
            int countOfCommentsOnHomePage = 4;
            var selectQuery = new StringBuilder();

            selectQuery.Append(" SELECT TOP( @countOfCommentsOnHomePage) c.UserId, c.Evaluation, c.Text, c.CreationDate, u.UserName ");
            selectQuery.Append(" FROM Comments as c INNER JOIN Users as u ON c.UserId = u.Id ");
            selectQuery.Append(" WHERE c.Evaluation >= @goodEvaluation AND c.Deleted != 1 ORDER BY c.Id desc ");

            return await Connection.QueryAsync<Comments, Users, CommentWithUserName>(selectQuery.ToString(),
                  map: (comment, user) => {
                      return new CommentWithUserName
                      {
                          UserId = comment.UserId,
                          UserName = user.UserName,
                          Evaluation = comment.Evaluation,
                          Text = comment.Text,
                          CreationDate = comment.CreationDate
                      };

                  }, splitOn: "UserName", param: new { countOfCommentsOnHomePage, goodEvaluation }, transaction: Transaction); ;
        }

        ///<inheritdoc/>
        public async Task<IEnumerable<CommentWithUserName>> GetCommentsWithUser(int housingId, int page)
        {
            int to = page * 20;
            int from = to - 20;
            var selectQuery = new StringBuilder();

            selectQuery.Append(" SELECT c.UserId, c.Evaluation, c.Text, c.CreationDate, u.UserName ");
            selectQuery.Append(" FROM Comments as c INNER JOIN Users as u ON c.UserId = u.Id ");
            selectQuery.Append(" WHERE c.HousingId = @housingId ORDER BY c.Id desc OFFSET @from ROWS FETCH NEXT @to ROWS ONLY ");

            var comments = await Connection.QueryAsync<Comments, Users, CommentWithUserName >(selectQuery.ToString(),
                map: ( comment, user) => {
                    return new CommentWithUserName
                    {
                        UserId = comment.UserId,
                        UserName = user.UserName,
                        Evaluation = comment.Evaluation,
                        Text = comment.Text,
                        CreationDate = comment.CreationDate
                    };

                }, splitOn: "UserName", param: new { housingId, from, to }, transaction: Transaction);

            return comments;

        }

        ///<inheritdoc/>
        public async Task<IEnumerable<CommentWithHousingInfo>> GetCommentsWithHousing(int userId, int page)
        {
            int to = page * 20;
            int from = to - 20;
            var sql = new StringBuilder();

            sql.Append(" SELECT c.Id, h.Id, h.TypeOfHousing, h.AdHeadline, h.Address, h.PriceNight, c.CreationDate, c.Evaluation, c.Text  ");
            sql.Append(" FROM Comments as c INNER JOIN Housing as h ON c.HousingId = h.Id ");
            sql.Append(" WHERE c.UserId = @userId ORDER BY c.Id desc OFFSET @from ROWS FETCH NEXT @to ROWS ONLY  ");

            var foundComments = await Connection.QueryAsync<Housing, Comments, CommentWithHousingInfo>(sql.ToString(),
                (housing, comment) => {
                    return new CommentWithHousingInfo
                    {
                        Address = housing.Address,
                        AdHeadline = housing.AdHeadline,
                        Text = comment.Text,
                        Id = comment.Id,
                        HousingId = housing.Id,
                        Evaluation = comment.Evaluation,
                        PriceNight = housing.PriceNight,
                        TypeOfHousing = housing.TypeOfHousing,
                        CreationDate = comment.CreationDate
                    };

                }, new { userId, from , to }, transaction: Transaction);

            return foundComments;
        }

        ///<inheritdoc/>
        public async Task<bool> IsCommentCreated( int userId, int housingId)
        {
            var selectQuery = new StringBuilder();
            selectQuery.Append("SELECT * FROM Comments as c INNER JOIN Users as u ON c.UserId = u.Id ");
            selectQuery.Append(" WHERE c.UserId = @userId and c.HousingId = @housingId ");

            var foundComments = await Connection.QueryAsync<Comments>(selectQuery.ToString(), new { userId, housingId}, transaction: Transaction);

            return foundComments.Any();
        }
    }
}
