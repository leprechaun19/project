﻿using Dapper;
using DAL.Entities;
using DAL.Entities.JoinEntities;
using DAL.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using Common.Enums;

namespace DAL.Repositories
{
    internal class BookingRepository : BaseRepository<Bookings>, IBookingRepository
    {
        public BookingRepository(IDbTransaction transaction) : base(transaction)
        {
        }

        ///<inheritdoc/>
        public async Task<int> CreateAndGetId(Bookings booking)
        {
            var createQuery = new StringBuilder();
            createQuery.Append(" INSERT INTO Bookings ( UserId, HousingId, Status, StartDate, EndDate, Cost, CreationDate, Deleted, ");
            createQuery.Append("Adults, ChildrenFrom6To17 )");
            createQuery.Append(" VALUES( @UserId, @HousingId, @Status, @StartDate, @EndDate, @Cost, @CreationDate, @Deleted, @Adults, @ChildrenFrom6To17 )");
            createQuery.Append(" SELECT CAST(SCOPE_IDENTITY() as int); ");

            var bookingId = await Connection.ExecuteScalarAsync<int>(createQuery.ToString(), booking, transaction: Transaction);

            return bookingId;
        }

        ///<inheritdoc/>
        public async Task Delete(int bookingId)
        {
            var deleteQuery = " UPDATE Bookings SET Deleted = 1 WHERE Id = @id  ";
            await Connection.ExecuteAsync(deleteQuery, new { bookingId }, transaction: Transaction);
        }

        ///<inheritdoc/>
        public async Task DeleteUserBookings(int userId)
        {
            var deleteQuery = " UPDATE Bookings SET Deleted = 1 WHERE UserId = @userId ";
            await Connection.ExecuteAsync(deleteQuery, userId, transaction: Transaction);
        }

        ///<inheritdoc/>
        public async Task<IEnumerable<ScheduleOfBooking>> GetBookingSchedule(int housingId)
        {
            var bookedPaidStatus = BookingStatus.BookedPaid;
            DateTime startDate = DateTime.Today.AddMonths(-1);
            var selectQuery = new StringBuilder();

            selectQuery.Append("SELECT * FROM Bookings as b ");
            selectQuery.Append(" inner join Users as u ");
            selectQuery.Append(" on b.UserId = u.Id ");
            selectQuery.Append(" where b.HousingId = @housingId and b.Deleted = 0 and ");
            selectQuery.Append(" b.StartDate >= @startDate and b.Status = @bookedPaidStatus ");

            var schedules = await Connection.QueryAsync<Bookings, Users, ScheduleOfBooking>(selectQuery.ToString(),
                           map: (booking, user) => {
                               return new ScheduleOfBooking
                               {
                                   UserId = user.Id,
                                   EndDate = booking.EndDate,
                                   StartDate = booking.StartDate,
                                   UserName = user.UserName
                               };

                           }, splitOn: "Id", param: new { housingId, startDate, bookedPaidStatus }, transaction: Transaction);

            return schedules;
        }

        ///<inheritdoc/>
        public async Task<IEnumerable<BookingWithHousingInfo>> GetBookingsWithHousing(int userId, int page)
        {
            int to = page * 20;
            int from = to - 20;
            var selectQuery = new StringBuilder();

            selectQuery.Append(" SELECT b.Id, b.Status, b.StartDate, b.EndDate, b.CreationDate, b.Cost, b.Adults, b.ChildrenFrom6To17,");
            selectQuery.Append("  h.Id, h.TypeOfHousing, h.AdHeadline, h.Address,  h.MainImgUrl ");
            selectQuery.Append(" FROM Bookings as b INNER JOIN Housing as h ON b.HousingId = h.Id ");
            selectQuery.Append(" WHERE b.UserId = @userId and b.Deleted = 0 ORDER BY b.Id desc OFFSET @from ROWS FETCH NEXT @to ROWS ONLY ");

            var bookings = await Connection.QueryAsync< Bookings, Housing, BookingWithHousingInfo>(selectQuery.ToString(),
                map: (booking, housing ) => {
                    return new BookingWithHousingInfo
                    {
                        Address = housing.Address,
                        AdHeadline = housing.AdHeadline,
                        HousingId = housing.Id,
                        TypeOfHousing = housing.TypeOfHousing,
                        Id = booking.Id,
                        Status = booking.Status,
                        Adults = booking.Adults,
                        ChildrenFrom6To17 = booking.ChildrenFrom6To17,
                        Cost = booking.Cost,
                        EndDate = booking.EndDate,
                        StartDate = booking.StartDate,
                        CreationDate = booking.CreationDate,
                        MainImgUrl = housing.MainImgUrl
                    };

                }, splitOn: "Id", param: new { userId, from, to }, transaction: Transaction);

            return bookings;

        }

        ///<inheritdoc/>
        public async Task ChangeBookingStatus(int bookingId, int status)
        {
            var updateQuery = " UPDATE Bookings SET Status = @status WHERE Id = @bookingId ";
            await Connection.ExecuteAsync(updateQuery, new { bookingId, status}, transaction: Transaction);
        }
    }
}
