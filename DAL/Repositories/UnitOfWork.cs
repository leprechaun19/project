﻿using DAL.Interfaces;
using DAL.Interfaces.Repositories;
using System;
using System.Data;
using System.Data.SqlClient;

namespace DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private IDbConnection dbConnection;
        private IDbTransaction transaction;
        private bool disposed = false;

        private BookingRepository _bookingRepository;
        private CommentRepository _commentRepository;
        private FavouriteRepository _favouriteRepository;
        private HousingRepository _housingRepository;
        private UserRepository _userRepository;

        public IBookingRepository Bookings =>
            _bookingRepository ??= new BookingRepository(transaction);
      
        public ICommentRepository Comments =>
            _commentRepository ??= new CommentRepository(transaction);

        public IFavouritesRepository Favourites =>
            _favouriteRepository ??= new FavouriteRepository(transaction);

        public IHousingRepository Houses =>
            _housingRepository ??= new HousingRepository(transaction);

        public IUserRepository Users => 
            _userRepository ??= new UserRepository(transaction);

        public UnitOfWork(string connectionString)
        {
            dbConnection = new SqlConnection(connectionString);
            dbConnection.Open();
            transaction = dbConnection.BeginTransaction();
        }

        /// <summary>
        /// Frees transaction and database objets.
        /// </summary>
        /// <param name="disposing">The parameter indicates whether resources are cleared.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (transaction != null)
                {
                    transaction.Dispose();
                    transaction = null;
                }

                if (dbConnection != null)
                {
                    dbConnection.Close();
                    dbConnection.Dispose();
                    dbConnection = null;
                }

                disposed = true;
            }
        }

        /// <summary>
        /// Frees unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~UnitOfWork()
        {
            Dispose(false);
        }

        ///<inheritdoc/>      
        public void SaveChanges()
        {
            try
            {
                transaction.Commit();
            }
            catch
            {
                transaction.Rollback();
                throw;
            }
            finally
            {
                transaction.Dispose();
                transaction = dbConnection.BeginTransaction();
                ResetRepositories();
            }
        }

        /// <summary>
        /// Resets the value of the repository variables to null.
        /// </summary>
        private void ResetRepositories()
        {
            _bookingRepository = null;
            _commentRepository = null;
            _favouriteRepository = null;
            _housingRepository = null;
            _userRepository = null;
        }
    }
}
