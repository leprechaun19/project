﻿using Common.Interfaces;
using Dapper.Contrib.Extensions;
using System;

namespace DAL.Entities
{
    public class Favourites : IEntity
    {
        [Key]
        public int Id { get; set; }
        public int UserId { get; set; }
        public int HousingId { get; set; }

        public DateTime CreationDate { get; set; }
    }
}
