﻿using Common.Enums;
using Common.Interfaces;
using System;

namespace DAL.Entities.JoinEntities
{
    public class BookingWithHousingInfo : IEntity
    {
        public int Id { get; set; }
        public int HousingId { get; set; }
        public string MainImgUrl { get; set; }


        public TypeOfHousing TypeOfHousing { get; set; }
        public string AdHeadline { get; set; }
        public string Address { get; set; }

        public BookingStatus Status { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CreationDate { get; set; }

        public double Cost { get; set; }

        public int Adults { get; set; }
        public int ChildrenFrom6To17 { get; set; }
    }
}
