﻿using System;

namespace DAL.Entities.JoinEntities
{
    public class ScheduleOfBooking
    {
        public string UserName { get; set; }
        public int UserId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
