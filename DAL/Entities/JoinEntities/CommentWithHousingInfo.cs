﻿using Common.Enums;
using Common.Interfaces;
using System;

namespace DAL.Entities.JoinEntities
{
     public class CommentWithHousingInfo : IEntity
    {
        public int Id { get; set; }
        public int HousingId { get; set; }

        public TypeOfHousing TypeOfHousing { get; set; }
        public string AdHeadline { get; set; }
        public string Address { get; set; }
        public double PriceNight { get; set; }

        public DateTime CreationDate { get; set; }

        public short Evaluation { get; set; }
        public string Text { get; set; }
    }
}
