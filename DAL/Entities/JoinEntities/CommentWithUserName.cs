﻿using System;

namespace DAL.Entities.JoinEntities
{
     public class CommentWithUserName
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public short Evaluation { get; set; }
        public string Text { get; set; }
        public DateTime CreationDate { get; set; }

    }
}
