﻿using Common.Enums;
using Common.Interfaces;
using System;

namespace DAL.Entities.JoinEntities
{
    public class FavouritesWithHousingInfo : IEntity
    {
        public int Id { get; set; }
        public int HousingId { get; set; }
        public string MainImgUrl { get; set; }

        public double PriceNight { get; set; }

        public int CommentAmount { get; set; }
        public TypeOfHousing TypeOfHousing { get; set; }
        public float AverageEvaluation { get; set; }
        public string AdHeadline { get; set; }
        public string Address { get; set; }

        public DateTime CreationDate { get; set; }
    }
}
