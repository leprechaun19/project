﻿using Common.Enums;
using Common.Interfaces;
using Dapper.Contrib.Extensions;
using System;

namespace DAL.Entities
{
    public class Users : IEntity
    {
        [Key]
        public int Id { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public DateTime? DOB { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string PasswordHash { get; set; }
        public string DynamicSalt { get; set; }

        public string Token { get; set; }

        public string AboutMe { get; set; }
        public Languages Language { get; set; }
        public DateTime CreationDate { get; set; }
        public bool Deleted { get; set; }
        public bool EmailConfirmed { get; set; }
        public bool PhoneConfirmed { get; set; }
        public bool IsAdmin { get; set; }
    }
}
