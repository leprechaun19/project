﻿using Common.Interfaces;
using Dapper.Contrib.Extensions;
using System;

namespace DAL.Entities
{
    public class Comments : IEntity
    {
        [Key]
        public int Id { get; set; }
        public int HousingId { get; set; }
        public int UserId { get; set; }

        public short Evaluation { get; set; }
        public string Text { get; set; }
        public DateTime CreationDate { get; set; }

        public bool Deleted { get; set; }
    }
}
