﻿using Common.Enums;
using Common.Interfaces;
using Dapper.Contrib.Extensions;
using System;

namespace DAL.Entities
{
    public class Bookings : IEntity
    {
        [Key]
        public int Id { get; set; }
        public int UserId { get; set; }
        public int HousingId { get; set; }

        public BookingStatus Status { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public double Cost { get; set; }
        public DateTime CreationDate { get; set; }
        public bool Deleted { get; set; }

        public int Adults { get; set; }
        public int ChildrenFrom6To17 { get; set; }
       
    }
}
