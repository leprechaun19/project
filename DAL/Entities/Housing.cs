﻿using Common.Enums;
using Common.Interfaces;
using Dapper.Contrib.Extensions;
using System;

namespace DAL.Entities
{
    public class Housing : IEntity
    {
        [Key]
        public int Id { get; set; }
        public int UserId { get; set; }

        public TypeOfHousing TypeOfHousing { get; set; }
        public string AdHeadline { get; set; }
        public string MainImgUrl { get; set; }
        public string Address { get; set; }
        public double PriceNight { get; set; }

        public float AverageEvaluation { get; set; }
        public int SumEvaluation { get; set; }
        public int CommentAmount { get; set; }

        public DateTime CreationDate { get; set; }
        public DateTime? UpdateDate { get; set; }

        public bool Deleted { get; set; }
        public bool Hidden { get; set; }

        public int NumberOfPossibleGuests { get; set; }
        public int NumberOfBeds { get; set; }
        public int SleepingPlaces { get; set; }
        public bool Bathroom { get; set; }
        public bool WiFi { get; set; }
        public bool BathAccessories { get; set; }
        public bool Television { get; set; }
        public bool Conditioner { get; set; }
        public bool Kitchen { get; set; }
        public bool Wardrobe { get; set; }
        public bool Microwave { get; set; }
        public bool WashingMachine { get; set; }

        public double? GeoLatitude { get; set; }
        public double? GeoLongitude { get; set; }
    }
}
