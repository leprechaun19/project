﻿using DAL.Interfaces.Repositories;
using System;

namespace DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IBookingRepository Bookings { get; }
        ICommentRepository Comments { get; }
        IFavouritesRepository Favourites { get; }
        IHousingRepository Houses { get; }
        IUserRepository Users { get; }

        /// <summary>
        /// Method commit or rollback transaction - saves changes in the database.
        /// </summary>
        void SaveChanges();
    }
}
