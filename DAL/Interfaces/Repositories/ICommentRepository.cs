﻿using DAL.Entities;
using DAL.Entities.JoinEntities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAL.Interfaces.Repositories
{
    public interface ICommentRepository : IRepository<Comments>
    {
        /// <summary>
        /// Gets the comment by user Id and housing Id.
        /// </summary>
        /// <param name="userId">The ID of the user.</param>
        /// <param name="housingId">The ID of the housing.</param>
        /// <returns>The found comment.</returns>
        Task<Comments> Get(int housingId, int userId);

        /// <summary>
        /// Gets some comments for Home page.
        /// </summary>
        /// <returns>A list of Comments.</returns>
        Task<IEnumerable<CommentWithUserName>> GetCommentsForIndex();

        /// <summary>
        /// Changes the field Deleted to the value true for all user's comments.
        /// </summary>
        /// <param name="userId">The ID of the user.</param>
        Task DeleteUsersComments(int userId);

        /// <summary>
        /// Changes the field Deleted to the value true for all housing's comments.
        /// </summary>
        /// <param name="housingId">The ID of the Housing.</param>
        Task DeleteHousesComments(int housingId);

        /// <summary>
        /// Gets a housing's comments with information about their User.
        /// </summary>
        /// <param name="housingId">The Id of housing.</param>
        /// <param name="page">The number of page.</param>
        /// <returns>The list of comments.</returns>
        Task<IEnumerable<CommentWithUserName>> GetCommentsWithUser(int housingId, int page);

        /// <summary>
        /// Gets a user's comments with information about their Housing.
        /// </summary>
        /// <param name="userId">The Id of user.</param>
        /// <param name="page">The number of page.</param>
        /// <returns>The list of comments.</returns>
        Task<IEnumerable<CommentWithHousingInfo>> GetCommentsWithHousing(int userId, int page);

        /// <summary>
        /// Checks if there is already a comment from a user to a specific housing.
        /// </summary>
        /// <param name="userId">The ID of the user.</param>
        /// <param name="housingId">The housing id.</param>
        /// <returns>The result of check.</returns>
        Task<bool> IsCommentCreated( int userId, int housingId);
    }
}
