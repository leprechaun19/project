﻿using DAL.Entities;
using DAL.Entities.JoinEntities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAL.Interfaces.Repositories
{
    public interface IBookingRepository : IRepository<Bookings>
    {
        /// <summary>
        /// Adds a booking to DB.
        /// </summary>
        /// <param name="booking">The booking to add.</param>
        /// <returns>The id of added booking.</returns>
        Task<int> CreateAndGetId(Bookings booking);

        /// <summary>
        /// Changes the field "Deleted" to the value true for all user's bookings.
        /// </summary>
        /// <param name="userId">The user ID.</param>
        Task DeleteUserBookings(int userId);

        /// <summary>
        /// Deletes a booking by his Id.
        /// </summary>
        /// <param name="bookingId">The ID of the booking.</param>
        Task Delete(int bookingId);

        /// <summary>
        /// Gets housing booking schedule for the near future.
        /// </summary>
        /// <param name="housingId">The Id of the housing.</param>
        /// <returns>The list of ScheduleOfBooking objects.</returns>
        Task<IEnumerable<ScheduleOfBooking>> GetBookingSchedule(int housingId);

        /// <summary>
        /// Gets a user's bookings with information about their Housing by user Id and page number.
        /// </summary>
        /// <param name="userId">The user ID.</param>
        /// <param name="page">The page number.</param>
        /// <returns>The list of BookingWithHousingInfo objects.</returns>
        Task<IEnumerable<BookingWithHousingInfo>> GetBookingsWithHousing(int userId, int page);

        /// <summary>
        /// Change the status of the booking.
        /// </summary>
        /// <param name="bookingId">The booking Id.</param>
        /// <param name="status">The new status of booking.</param>
        Task ChangeBookingStatus(int bookingId, int status);
    }
}
