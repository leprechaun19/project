﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAL.Interfaces.Repositories
{
    public interface IHousingRepository : IRepository<Housing>
    {
        /// <summary>
        /// Searches housings by some parameters.
        /// </summary>
        /// <param name="costFrom">The cost "From".</param>
        /// <param name="costTo">The cost "To".</param>
        /// <param name="Adult">The count of adults.</param>
        /// <param name="Child">The count of child.</param>
        /// <param name="SleepingPlaces">The count of sleeping places in housing.</param>
        /// <param name="Bathroom">Should there be a bathroom?</param>
        /// <param name="WiFi">Should there be a wi-fi?</param>
        /// <param name="BathAccessories">Should there be a bath accessories?</param>
        /// <param name="Television">Should there be a television?</param>
        /// <param name="Conditioner">Should there be a conditioner?</param>
        /// <param name="Kitchen">Should there be a kitchen?</param>
        /// <param name="Wardrobe">Should there be a wardrobe?</param>
        /// <param name="Microwave">Should there be a microwave?</param>
        /// <param name="WashingMachine">Should there be a washing machine?</param>
        /// <param name="page">The number of page.</param>
        /// <returns>Found houses.</returns>
        Task<IEnumerable<Housing>> SearchFilter(double costFrom, double costTo, int Adult, int Child, int SleepingPlaces, bool Bathroom,
            bool WiFi, bool BathAccessories, bool Television, bool Conditioner, bool Kitchen, bool Wardrobe, bool Microwave,
          bool WashingMachine, int page);

        /// <summary>
        ///  Changes the field Deleted to the value true by housing Id.
        /// </summary>
        /// <param name="housingId">The ID of the housing.</param>
        Task Delete(int housingId);

        /// <summary>
        /// Creates a housing and returns his id.
        /// </summary>
        /// <param name="housing">The housing to add.</param>
        /// <returns>The Id of added housing.</returns>
        Task<int> CreateAndGetId(Housing housing);

        /// <summary>
        /// Changes the field Hidden in the Housing to the value true.
        /// </summary>
        /// <param name="id">The ID of the housing.</param>
        Task HideAd(int id);

        /// <summary>
        /// Changes the field Hidden in the Housing to the value false.
        /// </summary>
        /// <param name="id">The ID of the housing.</param>
        Task ShowAd(int id);

        /// <summary>
        /// Gets a user's housings.
        /// </summary>
        /// <param name="userId">The user ID.</param>
        /// <param name="page">The page number.</param>
        /// <returns>The list of Housings.</returns>
        Task<IEnumerable<Housing>> GetUsersHouses(int userId, int page);

        /// <summary>
        /// Changes the field Deleted to the value true for all user's houses.
        /// </summary>
        /// <param name="userId">The ID of the user.</param>
        Task DeleteUsersHouses(int userId);

        /// <summary>
        /// Searches housings by headline and page number.
        /// </summary>
        /// <param name="headline">The headline of ad.</param>
        /// <param name="page">The page number.</param>
        /// <returns>The list of Housing objects.</returns>
        Task<IEnumerable<Housing>> SearchHouses(string headline, int page);

        /// <summary>
        /// Checks that user's dates for booking is free.
        /// </summary>
        /// <param name="housingId">The housing ID. </param>
        /// <param name="start">The start date of booking.</param>
        /// <param name="end">The end date of booking.</param>
        /// <returns>The result of check.</returns>
        Task<bool> IsDatesFree(int housingId, DateTime start, DateTime end);

        /// <summary>
        /// Adds new evaluation to Housing.
        /// </summary>
        /// <param name="housingId">The ID of the housing.</param>
        /// <param name="evaluation">The new evaluation of housing.</param>
        Task AddEvaluation(int housingId, int evaluation);

        /// <summary>
        /// Changes the evaluation of Housing item.
        /// </summary>
        /// <param name="housingId">The ID of the Housing.</param>
        /// <param name="newEvaluation">The new evaluation of Housing.</param>
        /// <param name="oldEvaluation">The old evaluation of Housing.</param>
        Task ChangeEvaluation(int housingId, int newEvaluation, int oldEvaluation);

    }
}
