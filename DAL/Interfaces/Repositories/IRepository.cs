﻿using DapperExtensions;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAL.Interfaces.Repositories
{
    /// <summary>
    /// The base interface for repositories that contain common methods for accessing data.
    /// </summary>
    public interface IRepository<T> where T : class
    {
        /// <summary>
        /// Gets item by his id.
        /// </summary>
        /// <param name="id">The identificator of item.</param>
        /// <returns>The found item.</returns>
        Task<T> GetById(int id);

        /// <summary>
        /// Creates item in DB.
        /// </summary>
        /// <param name="item">The new item.</param>
        Task Create(T item);

        /// <summary>
        /// Update item.
        /// </summary>
        /// <param name="item">The item for updating.</param>
        Task Update(T item);

        /// <summary>
        /// Deletes item.
        /// </summary>
        /// <param name="item">the item for deleting.</param>
        Task Delete(T item);

        /// <summary>
        /// Gets items by predicate of one field.
        /// </summary>
        /// <param name="predicate">Predicate for one model field.</param>
        /// <returns>Found elements.</returns>
        Task<IEnumerable<T>> GetByPredicate(IFieldPredicate predicate);

        /// <summary>
        /// Gets items by group of field predicates.
        /// </summary>
        /// <param name="predicateGroup">The group of field predicates.</param>
        /// <returns>The list of found elements.</returns>
        Task<IEnumerable<T>> GetByPredicateGroup(IPredicateGroup predicateGroup);

        /// <summary>
        ///  Gets count of items by group of field predicates.
        /// </summary>
        /// <param name="predicateGroup">The group of field predicates.</param>
        /// <returns>The count of found elements.</returns>
        Task<int> GetByPredicateGroupCount(IPredicateGroup predicateGroup);

        /// <summary>
        ///  Gets count of items by field predicate.
        /// </summary>
        /// <param name="predicate">The field predicate.</param>
        /// <returns>The count of found elements.</returns>
        Task<int> GetByPredicateCount(IFieldPredicate predicate);

        /// <summary>
        ///  Gets items on page by group of field predicates.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="predicateGroup">The group of field predicates.</param>
        /// <returns>The list of found elements.</returns>
        Task<IEnumerable<T>> GetPageByPredicateGroup(IPredicateGroup predicateGroup, int page);
    }
}
