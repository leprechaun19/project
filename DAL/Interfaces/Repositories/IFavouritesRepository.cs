﻿using DAL.Entities;
using DAL.Entities.JoinEntities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAL.Interfaces.Repositories
{
    public interface IFavouritesRepository : IRepository<Favourites>
    {
        /// <summary>
        /// Gets a user's favourites with information about their Housing.
        /// </summary>
        /// <param name="userId">The Id of the user.</param>
        /// <param name="page">The number of page.</param>
        /// <returns>The list of favourites.</returns>
        Task<IEnumerable<FavouritesWithHousingInfo>> GetFavouritesWithHousing(int userId, int page);
    }
}
