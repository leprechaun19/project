﻿using DAL.Entities;
using System.Threading.Tasks;

namespace DAL.Interfaces.Repositories
{
    public interface IUserRepository : IRepository<Users>
    {
        /// <summary>
        /// Adds a user and returned his Id.
        /// </summary>
        /// <param name="user">The user to add.</param>
        /// /// <returns>The Id of added user.</returns>
        Task<int> CreateAndGetId(Users user);

        /// <summary>
        /// Changes user's password hash in the database.
        /// </summary>
        /// <param name="user">The user with new password hash.</param>
        Task ChangePassword(Users user);
    }
}
