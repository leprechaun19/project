﻿namespace Common.Enums
{
    /// <summary>
    /// The type of housing.
    /// </summary>
    public enum TypeOfHousing : int
    {
        /// <summary>
        /// The house.
        /// </summary>
        Housing = 1,

        /// <summary>
        /// The apartament.
        /// </summary>
        Apartament = 2,

        /// <summary>
        /// Only room.
        /// </summary>
        Room = 3
    }
}
