﻿namespace Common.Enums
{
    /// <summary>
    /// The status of booking.
    /// </summary>
    public enum BookingStatus : int
    {
        /// <summary>
        /// NotBooked.
        /// </summary>
        NotBooked = 1,

        /// <summary>
        /// Booking in process.
        /// </summary>
        PreBooked = 2,

        /// <summary>
        /// Booked and paid.
        /// </summary>
        BookedPaid = 3,

        /// <summary>
        /// User canceled payment.
        /// </summary>
        CancelPayment
    }
}
