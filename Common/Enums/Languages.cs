﻿namespace Common.Enums
{
    /// <summary>
    /// The user selected language.
    /// </summary>
    public enum Languages : int
    {
        /// <summary>
        /// English.
        /// </summary>
        en = 1,

        /// <summary>
        /// Russian.
        /// </summary>
        ru = 2
    }
}
