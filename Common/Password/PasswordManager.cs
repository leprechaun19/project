﻿using Common.Hash;
using Common.Interfaces;
using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Common.Password
{
    public class PasswordManager : IPasswordManager
    {
        private readonly SHAHashCalculator _hashCalculator;
        private readonly string _globalSalt;

        public PasswordManager(string globalSalt)
        {
            _globalSalt = globalSalt;
            _hashCalculator = new SHAHashCalculator();
        }

        ///<inheritdoc/>
        public string GetPasswordSaltHash(string password, string dynamicSalt)
        {
            var passwordHashAndDS = GetHashAddition(password, dynamicSalt);
            var resultHash = GetHashAddition(passwordHashAndDS, _globalSalt);

            return resultHash;
        }

        ///<inheritdoc/>
        public bool VerifyHashedPassword(string passwordHash, string providedPassword, string dynamicSalt)
        {
            var hashedProvidedPassword = GetPasswordSaltHash(providedPassword, dynamicSalt);
            var result = hashedProvidedPassword.Equals(passwordHash);

            return result;
        }

        ///<inheritdoc/>
        public string GeneratePassword()
        {
            byte[] password = new byte[128 / 8];

            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(password);
            }

            return Convert.ToBase64String(password).Substring(0, 6);
        }

        /// <summary>
        /// Adds to input string addition and calculates its hash.
        /// </summary>
        /// <param name="input">The original string.</param>
        /// <param name="addition">The addition.</param>
        /// <returns>The hash of concated string.</returns>
        private string GetHashAddition(string input, string addition)
        {
            var firstAdditional = _hashCalculator.GetHashInBytes(input).Concat(Encoding.UTF8.GetBytes(addition)).ToArray();
            return _hashCalculator.GetStringHash(firstAdditional);
        }
    }
}
