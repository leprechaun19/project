﻿using System.Security.Cryptography;

namespace Common.Password
{
    public static class SaltGenerator
    {
        private static readonly int bitLenghtOfSalt = 32;

        /// <summary>
        /// Gets random salt 32 bits long.
        /// </summary>
        /// <returns>The random salt 32 bits long.</returns>
        public static byte[] GetRandomSalt()
        {
            byte[] salt = new byte[bitLenghtOfSalt];
            GenerateSalt(ref salt);

            return salt;
        }

        /// <summary>
        /// Fills an array of bytes with random bytes.
        /// </summary>
        /// <param name="salt">The empty array of bytes.</param>
        /// <returns>An array of bytes with random bytes.</returns>
        private static void GenerateSalt(ref byte[] salt)
        {
            var random = new RNGCryptoServiceProvider();

            random.GetNonZeroBytes(salt);
        }
    }
}
