﻿namespace Common.Interfaces
{
    public interface IPasswordManager
    {
        /// <summary>
        /// Сalculates a hash from the password with the addition of global and dynamic salt.
        /// </summary>
        /// <param name="password">The user password.</param>
        /// <param name="dynamicSalt">The user dynamic salt.</param>
        /// <returns>The string hash with global and dynamic salt.</returns>
        string GetPasswordSaltHash(string password, string dynamicSalt);

        /// <summary>
        /// Verifies provided user password.
        /// </summary>
        /// <param name="passwordHash">The password hash.</param>
        /// <param name="providedPassword">The provided password.</param>
        /// <param name="dynamicSalt">The dynamic salt.</param>
        /// <returns>The result of verification.</returns>
        bool VerifyHashedPassword(string passwordHash, string providedPassword, string dynamicSalt);

        /// <summary>
        /// Generates a random password.
        /// </summary>
        /// <returns>The string that represents the password (6 symbols).</returns>
        string GeneratePassword();
    }
}
