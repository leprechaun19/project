﻿
namespace Common.Models
{
    public class SMTPSettings
    {
        public string SenderName { get; set; }
        public string SenderAddress { get; set; }
        public string SenderPassword { get; set; }
        public string HostName { get; set; }
        public int Port { get; set; }
    }
}
