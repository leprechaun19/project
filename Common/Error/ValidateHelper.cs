﻿using System.Linq;

namespace Common.Error
{
    /// <summary>
    /// The class generate the methods, which contains internal error messages
    /// </summary>
    public static class ValidateHelper
    {
        private static readonly ErrorModel[] _codeOfErrors =
        {
            new ErrorModel(100, "InvalidData", false),

            new ErrorModel(101, "InvalidOldPassword", false),
            new ErrorModel(102, "UserBlockedDeleted", false),
            new ErrorModel(103, "EmailExist", false),
            new ErrorModel(104, "PhoneExist", false),
            new ErrorModel(105, "InvalidLoginOrPassword", false),
            new ErrorModel(106, "EmailNotConfirmed", false),

            new ErrorModel(201, "IsNotOwnerBooking", false),
            new ErrorModel(202, "OwnerBookingHisHousing", false),
            new ErrorModel(203, "DeleteBookingPaid", false),
            new ErrorModel(204, "BookingNotPaidForComment", false),
            new ErrorModel(205, "BookingNotEndedForComment", false),
            new ErrorModel(206, "BookedYet", false),
            new ErrorModel(207, "NumberPeopleInvalid", false),

            new ErrorModel(301, "IsNotOwnerHousing", false),
            new ErrorModel(302, "HousingDeleted", false),
            new ErrorModel(303, "CommentCreated", false),
            new ErrorModel(304, "InvalidFileContentType", false),

            new ErrorModel(401, "AddToFavSelf", false),
            new ErrorModel(402, "FavExistYet", false)
        };

        public static ErrorModel InvalidData
        {
            get
            {
                return _codeOfErrors.FirstOrDefault(x => x.Code == 100);
            }
        }

        #region UserException
        public static ErrorModel InvalidOldPassword
        {
            get
            {
                return _codeOfErrors.FirstOrDefault(x => x.Code == 101);
            }
        }

        public static ErrorModel UserBlockedOrDeleted
        {
            get
            {
                return _codeOfErrors.FirstOrDefault(x => x.Code == 102);
            }
        }

        public static ErrorModel EmailExist
        {
            get
            {
                return _codeOfErrors.FirstOrDefault(x => x.Code == 103);
            }
        }

        public static ErrorModel PhoneExist
        {
            get
            {
                return _codeOfErrors.FirstOrDefault(x => x.Code == 104);
            }
        }

        public static ErrorModel InvalidLoginOrPassword
        {
            get
            {
                return _codeOfErrors.FirstOrDefault(x => x.Code == 105);
            }
        }

        public static ErrorModel EmailNotConfirmed
        {
            get
            {
                return _codeOfErrors.FirstOrDefault(x => x.Code == 106);
            }
        }
        #endregion

        #region BookingException
        public static ErrorModel IsNotOwnerBooking
        {
            get
            {
                return _codeOfErrors.FirstOrDefault(x => x.Code == 201);
            }
        }

        public static ErrorModel OwnerBookingHisHousing
        {
            get
            {
                return _codeOfErrors.FirstOrDefault(x => x.Code == 202);
            }
        }

        public static ErrorModel DeleteBookingPaid
        {
            get
            {
                return _codeOfErrors.FirstOrDefault(x => x.Code == 203);
            }
        }

        public static ErrorModel BookingNotPaidForComment
        {
            get
            {
                return _codeOfErrors.FirstOrDefault(x => x.Code == 204);
            }
        }

        public static ErrorModel BookingNotEndedForComment
        {
            get
            {
                return _codeOfErrors.FirstOrDefault(x => x.Code == 205);
            }
        }

        public static ErrorModel BookedYet
        {
            get
            {
                return _codeOfErrors.FirstOrDefault(x => x.Code == 206);
            }
        }

        public static ErrorModel NumberPeopleInvalid
        {
            get
            {
                return _codeOfErrors.FirstOrDefault(x => x.Code == 207);
            }
        }
        #endregion

        #region HousingException
        public static ErrorModel IsNotOwnerHousing
        {
            get
            {
                return _codeOfErrors.FirstOrDefault(x => x.Code == 301);
            }
        }

        public static ErrorModel HousingDeleted
        {
            get
            {
                return _codeOfErrors.FirstOrDefault(x => x.Code == 302);
            }
        }

        public static ErrorModel CommentCreated
        {
            get
            {
                return _codeOfErrors.FirstOrDefault(x => x.Code == 303);
            }
        }

        public static ErrorModel InvalidFileContentType
        {
            get
            {
                return _codeOfErrors.FirstOrDefault(x => x.Code == 304);
            }
        }
        #endregion

        #region FavouriteException
        public static ErrorModel AddToFavSelf
        {
            get
            {
                return _codeOfErrors.FirstOrDefault(x => x.Code == 401);
            }
        }

        public static ErrorModel FavExistYet
        {
            get
            {
                return _codeOfErrors.FirstOrDefault(x => x.Code == 402);
            }
        }
        #endregion
    }
}
