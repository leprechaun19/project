﻿namespace Common.Error
{
    public class ErrorModel
    {
        public int Code { get; set; }
        public string Message { get; set; }

        /// <summary>
        /// The status of request - operation completed or not
        /// </summary>
        public bool Status { get; set; }

        public ErrorModel()
        {
        }

        public ErrorModel(bool status)
        {
            Status = status;
        }

        public ErrorModel(int code, string message)
        {
            Code = code;
            Message = message;
        }

        public ErrorModel(int code, string message, bool status)
        {
            Code = code;
            Message = message;
            Status = status;
        }
    }
}
