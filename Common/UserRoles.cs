﻿
namespace Common
{
    /// <summary>
    /// All possible roles in the app to access controllers.
    /// </summary>
    public static class UserRoles
    {
        public const string Admin = "Admin";
        public const string User = "User";
    }
}
