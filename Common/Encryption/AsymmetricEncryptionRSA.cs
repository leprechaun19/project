﻿using System.Security.Cryptography;

namespace Common.Encryption
{
    class AsymmetricEncryptionRSA
    {
        public RSAParameters PublicKey { get; set; }
        public RSAParameters PrivateKey { get; set; }

        public AsymmetricEncryptionRSA()
        {
            RSA Rsa = new RSACryptoServiceProvider();
            PublicKey = Rsa.ExportParameters(false);
            PrivateKey = Rsa.ExportParameters(true);
        }
        public AsymmetricEncryptionRSA(RSAParameters publicKey)
        {
            RSA Rsa = new RSACryptoServiceProvider();
            PublicKey = publicKey;
            PrivateKey = Rsa.ExportParameters(true);
        }
        public AsymmetricEncryptionRSA(RSAParameters publicKey, RSAParameters privateKey)
        {
            PublicKey = publicKey;
            PrivateKey = privateKey;
        }

        public byte[] Encrypt(byte[] plainText)
        {
            byte[] encrypted;

            using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider())
            {
                rsa.ImportParameters(PublicKey);
                encrypted = rsa.Encrypt(plainText, false);
            }

            return encrypted;
        }

        public byte[] Decrypt(byte[] cipherText)
        {
            byte[] encrypted;

            using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider())
            {
                rsa.ImportParameters(PrivateKey);
                encrypted = rsa.Decrypt(cipherText, false);
            }

            return encrypted;
        }
    }
}
