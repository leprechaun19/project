﻿using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Common.Encryption
{
    class SymmetricEncryptionAES
    {
        public CipherMode CipherMode { get; set; }
        public byte[] Key { get; set; }
        public byte[] InitializationVector { get; set; }

        public byte[] Encrypt(byte[] plainText)
        {
            RijndaelManaged rigAlg = new RijndaelManaged
            {
                Key = Key,
                IV = InitializationVector,
                Mode = CipherMode
            };

            ICryptoTransform encryptor = rigAlg.CreateEncryptor(rigAlg.Key, rigAlg.IV);

            return EncryptInStream(encryptor, plainText);

        }

        public string Decrypt(byte[] cipherText)
        {
            string plainText;

            RijndaelManaged rigAlg = new RijndaelManaged
            {
                Key = Key,
                IV = InitializationVector,
                Mode = CipherMode
            };

            ICryptoTransform decryptor = rigAlg.CreateDecryptor(rigAlg.Key, rigAlg.IV);

            plainText = DecryptFromStream(decryptor, cipherText);

            return plainText;
        }

        private byte[] EncryptInStream(ICryptoTransform encryptor, byte[] plainText)
        {
            byte[] encrypted;
            string plaintext = Encoding.Default.GetString(plainText);

            using (MemoryStream msEncrypt = new MemoryStream())
            {
                using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                {
                    using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                    {
                        swEncrypt.Write(plaintext);
                    }
                    encrypted = msEncrypt.ToArray();
                }
            }

            return encrypted;
        }

        private string DecryptFromStream(ICryptoTransform decryptor, byte[] cipherText)
        {
            string plainText = null;

            using (MemoryStream msDecrypt = new MemoryStream(cipherText))
            {
                using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                {
                    using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                    {
                        plainText = srDecrypt.ReadToEnd();
                    }
                }
            }

            return plainText;
        }
    }
}
