﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Common.Hash
{
    internal class SHAHashCalculator
    {
        private readonly SHA1 shaProvider;

        public SHAHashCalculator()
        {
            shaProvider = new SHA1CryptoServiceProvider();
        }

        /// <summary>
        /// Gets SHA hash in bytes from string.
        /// </summary>
        /// <param name="inputText">The string from which the hash will be calculated.</param>
        /// <returns>The hash from input value.</returns>
        public byte[] GetHashInBytes(string inputText)
        {
            byte[] stringInBytes = Encoding.UTF8.GetBytes(inputText);
            var hash = shaProvider.ComputeHash(stringInBytes);

            return hash;
        }

        /// <summary>
        /// Gets SHA string hash.
        /// </summary>
        /// <param name="inputBytes">The array of bytes from which the hash will be calculated.</param>
        /// <returns>The string hash from array of bytes.</returns>
        public string GetStringHash(byte[] inputBytes)
        {
            var hash = shaProvider.ComputeHash(inputBytes);
            var stringHash = Convert.ToBase64String(hash);

            return stringHash;
        }
    }
}
