﻿using Microsoft.AspNetCore.Http;
using Common.Error;
using RentalHousing.WEB.Models.Housing;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RentalHousing.WEB.Interfaces
{
    public interface IFileStorageService
    {
        /// <summary>
        /// Gets housing's files paths starting from the media folder.
        /// </summary>
        /// <param name="userId">The id of user.</param>
        /// <param name="housingId">The id of user's housing.</param>
        List<string> GetHousingFilesIfExist(int userId, int housingId);

        /// <summary>
        /// Save housing's main photo in wwwroot/Media/userId/housingId/.
        /// </summary>
        /// <param name="userId">The id of user.</param>
        /// <param name="housingId">The id of user's housing.</param>
        /// <param name="file">The housing's main file.</param>
        Task<string> SaveMainPhoto(int userId, int housingId, IFormFile file);

        /// <summary>
        /// Checks content type of housing added files.
        /// </summary>
        /// <param name="model">The housing with files.</param>
        /// <returns>Returned ErrorModel with message or null.</returns>
        ErrorModel ValidateFilesBeforeAdd(HousingUpdateViewModel model);

        /// <summary>
        /// Save housing's files in wwwroot/Media/userId/housingId/Images/.
        /// </summary>
        /// <param name="userId">The id of user.</param>
        /// <param name="housingId">The id of user's housing.</param>
        /// <param name="files">The list of housing's files.</param>
        Task SaveHousingFiles(int userId, int housingId, List<IFormFile> files);
    }
}
