﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Common.Error;
using RentalHousing.WEB.Resources;
using RentalHousing.WEB.Interfaces;
using RentalHousing.WEB.Models.Housing;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace RentalHousing.WEB.Services
{
    public class FileStorageService : IFileStorageService
    {
        private readonly string FileStoragePath;
        private readonly string FilesHousingPath;

        private readonly IWebHostEnvironment _webHostEnvironment;

        private readonly string[] AllowedContentTypes = {
                "image/jpeg", //jpg
                "image/png" //png
        };

        public FileStorageService(IWebHostEnvironment environment, string fileStoragePath, string filesHousingPath)
        {
            FilesHousingPath = filesHousingPath;
            FileStoragePath = fileStoragePath;
            _webHostEnvironment = environment;
        }

        ///<inheritdoc/>
        public async Task SaveHousingFiles(int userId, int housingId, List<IFormFile> files)
        {
            foreach (var file in files)
            {
                var fileStoragePath = Path.Combine(_webHostEnvironment.WebRootPath, FileStoragePath, userId.ToString(), housingId.ToString(), FilesHousingPath);
                await SaveFile(fileStoragePath, file);
            }
        }

        ///<inheritdoc/>
        public async Task<string> SaveMainPhoto(int userId, int housingId, IFormFile file)
        {
            string mainPhotoPath = default;

            if (file != null)
            {
                var fileStoragePath = Path.Combine(_webHostEnvironment.WebRootPath, FileStoragePath, userId.ToString(), housingId.ToString());
                await SaveFile(fileStoragePath, file);

                mainPhotoPath = string.Format(StringResources.MainPhotoPathPattern, FileStoragePath,
                    userId.ToString(), housingId.ToString(), file.FileName); ;
            }

            return mainPhotoPath;
        }

        /// <summary>
        /// Saves form file to current path.
        /// </summary>
        /// <param name="pathToSave">The path to file.</param>
        /// <param name="file">The form file.</param>
        private static async Task SaveFile(string pathToSave, IFormFile file)
        {
            EnsureDirExists(pathToSave);

            using var fileStream = new FileStream(Path.Combine(pathToSave, file.FileName), FileMode.Create);
            await file.CopyToAsync(fileStream);
        }

        ///<inheritdoc/>
        public List<string> GetHousingFilesIfExist(int userId, int housingId)
        {
            List<string> listOfFilesPaths = new();

            var mainImageFolderPath = Path.Combine(_webHostEnvironment.WebRootPath, FileStoragePath, userId.ToString(), housingId.ToString());
            var otherImagesFolderPath = Path.Combine(mainImageFolderPath, FilesHousingPath);

            if (Directory.Exists(mainImageFolderPath) && Directory.Exists(otherImagesFolderPath))
            {
                listOfFilesPaths.AddRange(GetFilesPathsInFolder(mainImageFolderPath));
                listOfFilesPaths.AddRange(GetFilesPathsInFolder(otherImagesFolderPath));
            }

            return listOfFilesPaths;
        }

        /// <summary>
        /// Gets files paths that starting from the Media local folder from specific folder. (/Media/1/11/cat.jpg)
        /// </summary>
        /// <param name="folderPath">the path to folder.</param>
        /// <returns>The list of paths to files.</returns>
        private static List<string> GetFilesPathsInFolder(string folderPath)
        {
            List<string> filesPathsFromTheMediaFolder = new();

            var filesFullPaths = Directory.GetFiles(folderPath);
            foreach (var path in filesFullPaths)
            {
                filesPathsFromTheMediaFolder.Add(path[path.IndexOf("\\Media")..].Replace("\\", "/"));
            }

            return filesPathsFromTheMediaFolder;
        }

        /// <summary>
        /// Create folder along the way if folder doesn't exist
        /// </summary>
        /// <param name="folderPath">Path to the folder</param>
        private static void EnsureDirExists(string folderPath)
        {
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
        }

        ///<inheritdoc/>
        public ErrorModel ValidateFilesBeforeAdd(HousingUpdateViewModel model)
        {
            if (!AllowedContentTypes.Contains(model.MainImg.ContentType))
            {
                return ValidateHelper.InvalidFileContentType;
            }

            foreach (var file in model.AdditionalFiles)
            {
                if (!AllowedContentTypes.Contains(file.ContentType))
                {
                    return ValidateHelper.InvalidFileContentType;
                }
            }

            return null;
        }
    }
}
