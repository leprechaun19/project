﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;
using RentalHousing.WEB.Resources;

namespace RentalHousing.WEB.Filters
{
    public class ExceptionFilter : ExceptionFilterAttribute
    {
        private readonly ILogger<ExceptionFilter> _logger;


        public ExceptionFilter(ILogger<ExceptionFilter> logger)
        {
            _logger = logger;
        }

        public override void OnException(ExceptionContext context)
        {
            string actionName = context.ActionDescriptor.DisplayName;
            string exceptionStack = context.Exception.StackTrace;
            string exceptionMessage = context.Exception.Message;

            _logger.LogError(StringResources.GlobalErrorMessageTemplate, 
                actionName, exceptionMessage, exceptionStack);

            context.Result = new RedirectToRouteResult(new RouteValueDictionary{
                                  { "controller", "Error" },
                                  { "action", "ServerError" }
                    });
        }
    }
}
