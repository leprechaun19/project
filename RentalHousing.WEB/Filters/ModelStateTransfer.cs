﻿using Microsoft.AspNetCore.Mvc.Filters;

namespace RentalHousing.WEB.Filters
{
    public abstract class ModelStateTransfer : ActionFilterAttribute
    {
        protected const string Key = nameof(ModelStateTransfer);
    }
}
