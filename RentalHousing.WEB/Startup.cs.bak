﻿using System;
using System.Collections.Generic;
using System.Globalization;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using RentalHousing.BLL.Infastructure;
using RentalHousing.WEB.Infastructure;
using RentalHousing.BLL.Utils;
using RentalHousing.WEB.Utils;
using System.Reflection;
using RentalHousing.WEB.Models;

namespace RentalHousing.WEB
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        
        public void ConfigureServices(IServiceCollection services)
        {
            var appSettingsSection = Configuration.GetSection("AppSettings");
            var appSettings = appSettingsSection.Get<AppSettings>();

            var settingsSection = Configuration.GetSection("Settings");
            var settings = settingsSection.Get<Settings>();

            services.Configure<Settings>(settingsSection);
            services.Configure<AppSettings>(appSettingsSection);

            #region CookiePolice
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            #endregion

            #region RequestLocalization
            services.Configure<RequestLocalizationOptions>(opts =>
            {
                var supportedCultures = new List<CultureInfo> {
                    new CultureInfo("en-GB"),
                    new CultureInfo("en-US"),
                    new CultureInfo("en"),
                    new CultureInfo("ru-RU"),
                    new CultureInfo("ru"),
                    };

               opts.DefaultRequestCulture = new RequestCulture("en");
               opts.SupportedCultures = supportedCultures;
               opts.SupportedUICultures = supportedCultures;
           });
            #endregion

            #region Authentication
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options => 
                {
                    options.Cookie.HttpOnly = true;
                    options.Cookie.Expiration = TimeSpan.FromDays(150);
                    options.SlidingExpiration = true;
                    options.LoginPath = new PathString("/GuestHome/Index");
                    options.AccessDeniedPath = new PathString("/Home/Index");
                    
                });
            #endregion

            #region AutoMapper Configuration
            services.AddOptions();
            services.AddAutoMapper(typeof(BLLMappingProfile), typeof(WEBMappingProfile));
            #endregion

            #region Services

            services.AddBllServices(Configuration.GetConnectionString("DefaultConnection"));
            services.AddBusinessLayerServices(appSettings.GlobalSalt);
            services.AddPLServices();

            #endregion

            #region Mvc
            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                .AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix, opts => { opts.ResourcesPath = "Resources"; })
                .AddDataAnnotationsLocalization(options =>
                {
                    var type = typeof(SharedResource);
                    var assemblyName = new AssemblyName(type.GetTypeInfo().Assembly.FullName);
                    options.DataAnnotationLocalizerProvider = (t, f) => f.Create("SharedResource", assemblyName.Name);
                });
            #endregion

            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseAuthentication();

            var options = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            app.UseRequestLocalization(options.Value);

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
