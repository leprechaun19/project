using System;
using System.Collections.Generic;
using System.Globalization;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using BLL.Infastructure;
using RentalHousing.WEB.Infastructure;
using BLL.Utils;
using RentalHousing.WEB.Utils;
using System.Reflection;
using RentalHousing.WEB.Models;
using Common;
using Newtonsoft.Json.Serialization;
using Microsoft.Extensions.Hosting;
using Common.Models;

namespace RentalHousing.WEB
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            HostingEnvironment = env;
        }

        public IWebHostEnvironment HostingEnvironment { get; set; }
        public IConfiguration Configuration { get; }
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddRazorPages();

            var appSettingsSection = Configuration.GetSection("AppSettings");
            var smtpSettingsSection = Configuration.GetSection("SMTPSettings");

            var appSettings = appSettingsSection.Get<AppSettings>();
            var smtpSettings = appSettingsSection.Get<SMTPSettings>();

            services.Configure<AppSettings>(appSettingsSection);
            services.AddSession();

            #region Authorization

            services.AddAuthorization(options =>
            {
                options.AddPolicy(UserRoles.Admin, policy => policy.RequireClaim(UserRoles.Admin));
                options.AddPolicy(UserRoles.User, policy => policy.RequireClaim(UserRoles.User));
            });

            #endregion

            #region CookiePolice
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.Lax;
            });
            #endregion

            #region RequestLocalization
            services.Configure<RequestLocalizationOptions>(opts =>
            {
                var supportedCultures = new List<CultureInfo> {
                    new CultureInfo("en-GB"),
                    new CultureInfo("en-US"),
                    new CultureInfo("en"),
                    new CultureInfo("ru-RU"),
                    new CultureInfo("ru"),
                    };

                opts.DefaultRequestCulture = new RequestCulture("en");
                opts.SupportedCultures = supportedCultures;
                opts.SupportedUICultures = supportedCultures;
                opts.RequestCultureProviders = new List<IRequestCultureProvider>{
                    new CookieRequestCultureProvider()};
            });
            #endregion

            #region Authentication
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options => 
                {
                    options.Cookie.HttpOnly = true;
                    options.ExpireTimeSpan = TimeSpan.FromDays(150);
                    options.SlidingExpiration = true;
                    options.LoginPath = new PathString("/GuestHome/Index");
                    options.AccessDeniedPath = new PathString("/Home/DetermineRole");
                });
            #endregion

            #region AutoMapper Configuration
            services.AddOptions();
            services.AddAutoMapper(typeof(BLLMappingProfile), typeof(WEBMappingProfile));
            #endregion

            #region Services

            services.AddDALServices(Configuration.GetConnectionString("DefaultConnection"));
            services.AddBLLServices(appSettings.GlobalSalt, smtpSettings);
            services.AddPLServices(HostingEnvironment, appSettings);

            #endregion

            #region Mvc
            services.AddMvc()
                .AddNewtonsoftJson(options => options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver())
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0)
                .AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix, opts => { opts.ResourcesPath = "Resources"; })
                .AddDataAnnotationsLocalization(options =>
                {
                    var type = typeof(SharedResource);
                    var assemblyName = new AssemblyName(type.GetTypeInfo().Assembly.FullName);
                    options.DataAnnotationLocalizerProvider = (t, f) => f.Create("SharedResource", assemblyName.Name);
                });
            #endregion
           
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseSession();
            app.UseRouting();

            app.UseCookiePolicy();

            app.UseAuthentication();
            app.UseAuthorization();

            var options = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            app.UseRequestLocalization(options.Value);

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapRazorPages();
                endpoints.MapControllerRoute("default", "{controller=Home}/{action=DetermineRole}/{id?}");
            });
        }
    }
}
