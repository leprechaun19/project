﻿$(document).ready(
    function () {
        $('#FavButton').click(function (a) {
            var housingId = $('#housingId').val();
            $.ajax({
                type: 'POST',
                contentType: "application/json",
                url: '/Favourite/Create/',
                dataType: "json",
                data: JSON.stringify({HousingId: housingId}),
                success: function (data) {
                    if (data.result.Status) {
                        $('#notFav').css("display", "none");
                        $('#notFav_text').css("display", "none");
                        $('#Fav').show();
                        $('#Fav_text').show();
                    }
                    else {
                        Swal.fire(
                            Error,
                            data.result.Message,
                            'error'
                        );
                    }  
                }
            });
        });
})