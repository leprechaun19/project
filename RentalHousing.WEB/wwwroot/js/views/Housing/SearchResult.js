﻿$(document).ready(
    function () {
        $('.aPage').click(function (a) {
            var page = $(this).text();
            $('#pageNumber').val(page);
            $('#searchForm').submit();
        });

        $("#Slider").slider({
            range: true,
            min: 1,
            max: 1000,
            values: [1, 1000],
            slide: function (event, ui) {
                $("#Amount").val("$" + ui.values[0] + " - $" + ui.values[1]);
            }
        });
        if ($("#Amount").val() == "") {
            $("#Amount").val("$" + $("#Slider").slider("values", 0) +
                " - $" + $("#Slider").slider("values", 1));
        }
})