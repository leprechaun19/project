﻿$(document).ready(
    function () {
        DevExpress.localization.locale(navigator.language);
    })

function deleteAd(adId, deleteAd, yes, no, deleting) {
    Swal.fire({
        title: deleteAd,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#28a745',
        cancelButtonColor: '#d33',
        confirmButtonText: yes,
        cancelButtonText: no,
    }).then((result) => {
        if (result.value) {
            Swal.fire({
                title: deleting,
                onBeforeOpen: () => {
                    Swal.showLoading()
                }
            })
            $.ajax({
                type: 'DELETE',
                url: '/Housing/Delete/' + adId,
                success: function (data) {
                    if (data.result.Status) {
                        location.href = '/Home/MyAds' ;
                    }
                    else {
                        Swal.fire(
                            Error,
                            data.result.Message,
                            'error'
                        );
                    }
                }
            });
        }
    })
}