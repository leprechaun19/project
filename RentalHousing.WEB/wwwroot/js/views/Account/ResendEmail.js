﻿function sendMail(userId, Success, EmailSend, Error) {
    $.ajax({
        type: 'GET',
        url: '/Account/ResendMail/' + userId,
        success: function (data) {
            Swal.fire(
                Success,
                EmailSend,
                'success'
            );
        },
        error: function (error) {
            Swal.fire(
                Error,
                "",
                'error'
            )
        }
    });
}