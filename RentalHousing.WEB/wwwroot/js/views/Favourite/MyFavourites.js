﻿function deleteFavourite(favId, deleteFav, yes, no, deleting) {
    Swal.fire({
        title: deleteFav,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#28a745',
        cancelButtonColor: '#d33',
        confirmButtonText: yes,
        cancelButtonText: no,
    }).then((result) => {
        if (result.value) {
            Swal.fire({
                title: deleting,
                onBeforeOpen: () => {
                    Swal.showLoading()
                }
            })
            $.ajax({
                type: 'DELETE',
                url: '/Favourite/Delete/' + favId,
                success: function (data) {
                    location.reload();
                }
            });
        }
    })
}