﻿using AutoMapper;
using BLL.DTO;
using RentalHousing.WEB.Models.Account;
using RentalHousing.WEB.Models.Booking;
using RentalHousing.WEB.Models.Comment;
using RentalHousing.WEB.Models.Favourite;
using RentalHousing.WEB.Models.Home;
using RentalHousing.WEB.Models.Housing;
using RentalHousing.WEB.Models.User;

namespace RentalHousing.WEB.Utils
{
    public class WEBMappingProfile : Profile
    {
        public WEBMappingProfile()
        {
            //
            CreateMap<BookingIndexDTO, BookingIndexViewModel>()
                .ForMember(view => view.Cost, opt => opt.MapFrom(dto => dto.Cost))
                .ForMember(view => view.Adults, opt => opt.MapFrom(dto => dto.Adults))
                .ForMember(view => view.ChildrenFrom6To17, opt => opt.MapFrom(dto => dto.ChildrenFrom6To17))
                .ForPath(view => view.Housing.Id, opt => opt.MapFrom(dto => dto.Housing.Id))
                .ForPath(view => view.Housing.Address, opt => opt.MapFrom(dto => dto.Housing.Address))
                .ForPath(view => view.Housing.AdHeadline, opt => opt.MapFrom(dto => dto.Housing.AdHeadline))
                .ForPath(view => view.Housing.TypeOfHousing, opt => opt.MapFrom(dto => dto.Housing.TypeOfHousing))
                .ForPath(view => view.Comment.Comment, opt => opt.MapFrom(dto => dto.Comment.Comment))
                .ForPath(view => view.Comment.Evaluation, opt => opt.MapFrom(dto => dto.Comment.Evaluation))
                .ForPath(view => view.Comment.HousingId, opt => opt.MapFrom(dto => dto.Comment.HousingId))
                .ForPath(view => view.Comment.Id, opt => opt.MapFrom(dto => dto.Comment.Id))
                .ForPath(view => view.Comment.UserId, opt => opt.MapFrom(dto => dto.Comment.UserId));
            //
            CreateMap<BookingCreateViewModel, BookingCreateDTO>();
            //
            CreateMap<BookingUpdateViewModel, BookingUpdateDTO>();
            //
            CreateMap<CommentCreateViewModel, CommentCreateDTO>();
            //
            CreateMap<CommentUpdateViewModel, CommentUpdateDTO>();
            //
            CreateMap<FavouriteCreateViewModel, FavouriteCreateDTO>();
            //
            CreateMap<HousingIndexDTO, HousingIndexViewModel>()
                .ForMember(view => view.Comments, opt => opt.MapFrom(dto => dto.Comments))
                .ForMember(view => view.User, opt => opt.MapFrom(dto => dto.User));
            //
            CreateMap<HousingUpdateDTO, HousingUpdateViewModel>();
                //
             CreateMap<HousingUpdateViewModel, HousingUpdateDTO>();
            //
            CreateMap<UserHousingDTO, UserHousingViewModel>()
                 .ForMember(c => c.Address, opt => opt.MapFrom(model => model.Address))
                 .ForMember(c => c.AdHeadline, opt => opt.MapFrom(model => model.AdHeadline))
                 .ForMember(c => c.Bathroom, opt => opt.MapFrom(model => model.Bathroom))
                 .ForMember(c => c.Id, opt => opt.MapFrom(model => model.Id))
                 .ForMember(c => c.NumberOfBeds, opt => opt.MapFrom(model => model.NumberOfBeds))
                 .ForMember(c => c.NumberOfPossibleGuests, opt => opt.MapFrom(model => model.NumberOfPossibleGuests))
                 .ForMember(c => c.PriceNight, opt => opt.MapFrom(model => model.PriceNight))
                 .ForMember(c => c.Schedules, opt => opt.MapFrom(model => model.Schedules))
                 .ForMember(c => c.SleepingPlaces, opt => opt.MapFrom(model => model.SleepingPlaces))
                 .ForMember(c => c.TypeOfHousing, opt => opt.MapFrom(model => model.TypeOfHousing));
            //
            CreateMap<UserHousingDTO, UserHousingViewModel>();
            //
            CreateMap<CommentHousingDTO,CommentWithUserVM>()
                .ForMember(vm => vm.UserName, opt => opt.MapFrom(dto => dto.UserName))
                .ForMember(vm => vm.UserId, opt => opt.MapFrom(dto => dto.UserId))
                .ForMember(vm => vm.Evaluation, opt => opt.MapFrom(dto => dto.Evaluation))
                .ForMember(vm => vm.CreationDate, opt => opt.MapFrom(dto => dto.CreationDate))
                .ForMember(vm => vm.Comment, opt => opt.MapFrom(dto => dto.Comment));
            //
            CreateMap<LogInViewModel, UserLogInDTO>()
                .ForMember(dto => dto.Email, opt => opt.MapFrom(model => model.UserEmail))
                 .ForMember(dto => dto.Password, opt => opt.MapFrom(model => model.UserPassword));
            //
            CreateMap<RegisterViewModel, UserCreateDTO>()
                .ForMember(dto => dto.UserName, opt => opt.MapFrom(model => model.UserName))
                 .ForMember(dto => dto.Phone, opt => opt.MapFrom(model => model.Phone))
                 .ForMember(dto => dto.Email, opt => opt.MapFrom(model => model.Email));
            //
            CreateMap<UserProfileDTO, UserProfileViewModel>();
            //
            CreateMap< UserUpdateDTO, UserUpdateViewModel>();
            //
            CreateMap<UserUpdateViewModel, UserUpdateDTO>();
            //
            CreateMap <UserProfileDTO, UserProfileViewModel>();
            //
            CreateMap<UserPersonalProfileDTO, MyAdsViewModel>()
                 .ForMember(view => view.Id, opt => opt.MapFrom(dto => dto.Id))
                 .ForMember(view => view.Houses, opt => opt.MapFrom(dto => dto.Houses));
            //
            CreateMap<UserPersonalProfileDTO, MyFavouritesViewModel>()
                 .ForMember(view => view.Id, opt => opt.MapFrom(dto => dto.Id))
                 .ForMember(view => view.Favourites, opt => opt.MapFrom(dto => dto.Favourites));
            //
            CreateMap<UserPersonalProfileDTO, MyBookingsViewModel>()
                 .ForMember(view => view.Id, opt => opt.MapFrom(dto => dto.Id))
                 .ForMember(view => view.Bookings, opt => opt.MapFrom(dto => dto.Bookings));
            //
            CreateMap<UserPersonalProfileDTO, MyCommentsViewModel>()
                 .ForMember(view => view.Id, opt => opt.MapFrom(dto => dto.Id))
                 .ForMember(view => view.Comments, opt => opt.MapFrom(dto => dto.Comments));
            //
            CreateMap<ChangePasswordViewModel, ChangePasswordDTO>()
                 .ForMember(view => view.OldPassword, opt => opt.MapFrom(dto => dto.OldPassword))
                 .ForMember(view => view.NewPassword, opt => opt.MapFrom(dto => dto.NewPassword));
        }
    }
}
