﻿using System.Security.Claims;
using System.Security.Principal;

namespace RentalHousing.WEB.Utils
{
    public static class IdentityExtension
    {
        public static int GetUserId(this IIdentity identity)
        {
            int userId = 0;

            if (identity is ClaimsIdentity claimsIdentity)
            {
                Claim claim = claimsIdentity?.FindFirst(x => x.Type == ClaimTypes.NameIdentifier);
                if (claim != null)
                {
                    userId = int.Parse(claim.Value);
                }
            }

            return userId;
        }
    }
}
