﻿using Microsoft.IdentityModel.Tokens;
using BLL.DTO;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Text;

namespace RentalHousing.WEB.Utils
{
    public static class TokenGenerator
    {
        /// <summary>
        /// Generates token based on user's data (email, name, phone).
        /// </summary>
        /// <param name="user">The user model.</param>
        /// <param name="expireDate">The expire date.</param>
        /// <returns>The generated token.</returns>
        public static string GenerateToken(UserCreateDTO user, DateTime expireDate)
        {
            var key = Encoding.ASCII.GetBytes($"{user.Email}{user.UserName}{user.Phone}");

            var jwtSecurityToken = new JwtSecurityToken(
                issuer: "RentalHousing",
                audience: user.UserName,
                notBefore: DateTime.UtcNow,
                expires: expireDate,
                signingCredentials: new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256));

            return new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken);
        }
    }
}
