﻿using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Mvc;
using BLL.Interfaces.IService;

namespace RentalHousing.WEB.Controllers.API
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UserAPIController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserAPIController(IUserService userService)
        {
            _userService = userService;
        }

        // GET: api/<UserController>
        [HttpGet]
        public object GetUsers(DataSourceLoadOptions loadOptions)
        {
            //_userService.get;
            //return DataSourceLoader.LoadAsync(baseQueryable, loadOptions);
            return 0;
        }

        // GET api/<UserController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<UserController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<UserController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<UserController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
