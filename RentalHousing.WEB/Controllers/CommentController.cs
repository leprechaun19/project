﻿using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using BLL.DTO;
using BLL.Interfaces.IService;
using BLL.Interfaces.Validation;
using RentalHousing.WEB.Filters;
using RentalHousing.WEB.Models.Comment;
using RentalHousing.WEB.Utils;

namespace RentalHousing.WEB.Controllers
{
    [Authorize]
    [ServiceFilter(typeof(ExceptionFilter))]
    [Route("[controller]/[action]")]
    public class CommentController : Controller
    {
        private readonly ICommentValidation _commentValidation;
        private readonly ICommentService _commentService;
        private readonly IMapper _mapper;

        public CommentController(ICommentService service, IMapper mapper, ICommentValidation commentValidation)
        {
            _mapper = mapper;
            _commentService = service;
            _commentValidation = commentValidation;
        }

        [HttpPost]
        [SetTempDataModelStateFilte]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(CommentCreateViewModel model)
        {
            int userId = User.Identity.GetUserId();
            var errorModel = await _commentValidation.ValidateCommentToAdd(model.BookingId, userId, model.HousingId);
            if (errorModel == null)
            {
                var commentCreate = _mapper.Map<CommentCreateViewModel, CommentCreateDTO>(model);
                commentCreate.UserId = userId;
                await _commentService.Create(commentCreate);
                return RedirectToAction("MyBookings", "Home");
            }

            return View("~/Views/Error/Error.cshtml", errorModel);
        }

        [HttpDelete]
        public async Task<ActionResult> Delete([FromQuery]int commentId)
        {
            await _commentService.Delete(commentId);
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ModelStateValidationFilter]
        public async Task<ActionResult> Update(CommentUpdateViewModel updateModel)
        {
            updateModel.UserId = User.Identity.GetUserId();

            var updateDTO = _mapper.Map<CommentUpdateViewModel, CommentUpdateDTO>(updateModel);
            await _commentService.Update(updateDTO);

            return RedirectToAction("MyBookings", "Home");
        }
    }
}
