﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using BLL.DTO;
using BLL.Interfaces.IService;
using BLL.Interfaces.Validation;
using Common.Enums;
using RentalHousing.WEB.Filters;
using RentalHousing.WEB.Models.Booking;
using RentalHousing.WEB.Utils;
using System.Threading.Tasks;

namespace RentalHousing.WEB.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    [ServiceFilter(typeof(ExceptionFilter))]
    public class BookingController : Controller
    {
        private readonly IBookingValidation _bookingValidation;
        private readonly IBookingService _bookingService;
        private readonly IMapper _mapper;

        public BookingController(IBookingService service, IMapper mapper, IBookingValidation bookingValidation)
        {
            _mapper = mapper;
            _bookingService = service;
            _bookingValidation = bookingValidation;
        }

        [HttpGet]
        [RestoreModelStateFromTempDataFilter]
        public async Task<ActionResult> Get(int bookingId)
        {
            var booking = await _bookingService.GetById(bookingId);
            var viewModel = _mapper.Map<BookingIndexViewModel>(booking);

            return View(viewModel);
        }

        [HttpGet]
        public async Task<ActionResult> Cancel([FromQuery]int bookingId)
        {
            await _bookingService.ChangeStatus(bookingId, BookingStatus.CancelPayment);
            return RedirectToAction("MyBookings", "Home");
        }

        [HttpPost]
        [SetTempDataModelStateFilte]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Pay(int bookingId)
        {
            var errorModel = await _bookingValidation.CheckDatesIsFree(bookingId);
            if (errorModel != null)
            {
                ViewData["error"] = errorModel.Message;
                var booking = await _bookingService.GetById(bookingId);

                return View("Payment", new PaymentViewModel { BookingId = booking.Id, Cost = booking.Cost });
            }

            await _bookingService.ChangeStatus(bookingId, BookingStatus.BookedPaid);
            return RedirectToAction("MyBookings", "Home");
        }

        [HttpPost]
        [SetTempDataModelStateFilte]
        [ValidateAntiForgeryToken]
        [ModelStateValidationFilter]
        public async Task<ActionResult> Create([FromForm]BookingCreateViewModel model)
        {
            model.UserId = User.Identity.GetUserId();
            double cost = model.Price * model.EndDate.Subtract(model.StartDate).Days;
            
            var bookingDTO = _mapper.Map<BookingCreateViewModel, BookingCreateDTO>(model);
            bookingDTO.Cost = cost;

            var errorModel = await _bookingValidation.ValidateBookingToCreate(bookingDTO, model.StartDate, model.EndDate);
            if(errorModel == null)
            {
                int id = await _bookingService.Create(bookingDTO);

                return View("Payment", new PaymentViewModel { BookingId = id, Cost = cost });
            }

            return View("~/Views/Error/Error.cshtml", errorModel);
        }

        [HttpGet]
        public async Task<ActionResult> Delete([FromQuery]int bookingId)
        {
            int userId = User.Identity.GetUserId();
            var errorModel = await _bookingValidation.ValidateBookingToDelete(bookingId, userId);
            if (errorModel == null)
            {
                await _bookingService.Delete(bookingId);
                return RedirectToAction("MyBookings", "Home");
            }

            return View("~/Views/Error/Error.cshtml", errorModel);
        }
    }
}
