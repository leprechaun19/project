﻿using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RentalHousing.WEB.Controllers
{
    [Route("[controller]/[action]")]
    public class ErrorController : Controller
    {
        [HttpGet]
        public new ActionResult NotFound()
        {
            Response.StatusCode = 404;
            return View();
        }

        [HttpGet]
        public ActionResult ServerError(object args)
        {
            Response.StatusCode = 500;
            return View();
        }
    }
}
