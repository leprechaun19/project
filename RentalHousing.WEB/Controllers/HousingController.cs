﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using BLL.DTO;
using BLL.Interfaces.IService;
using BLL.Interfaces.Validation;
using Common.Error;
using RentalHousing.WEB.Filters;
using RentalHousing.WEB.Interfaces;
using RentalHousing.WEB.Models.Housing;
using RentalHousing.WEB.Resources;
using RentalHousing.WEB.Utils;
using System.Threading.Tasks;

namespace RentalHousing.WEB.Controllers
{
    [Authorize]
    [ServiceFilter(typeof(ExceptionFilter))]
    [Route("[controller]/[action]")]
    [ServiceFilter(typeof(ExceptionFilter))]
    public class HousingController : Controller
    {
        private readonly SharedViewLocalizer _localizer;
        private readonly IHousingValidation _housingValidation;
        private readonly IHousingService _housingService;
        private readonly IMapper _mapper;
        private readonly IFileStorageService _fileStorageService;

        public HousingController(IHousingService service, IMapper mapper, IHousingValidation housingValidation, 
            IFileStorageService fileStorageService, SharedViewLocalizer localizer)
        {
            _localizer = localizer;
            _mapper = mapper;
            _housingService = service;
            _housingValidation = housingValidation;
            _fileStorageService = fileStorageService;
        }

        [HttpGet]
        [RestoreModelStateFromTempDataFilter]
        public ActionResult Create()
        {
            return View();
        }


        [HttpGet]
        [RestoreModelStateFromTempDataFilter]
        public async Task<ActionResult> Update(int housingId)
        {
            var housing = await _housingService.GetForUpdate(housingId);
            var viewModel = _mapper.Map<HousingUpdateDTO, HousingUpdateViewModel>(housing);

            return View("Update", viewModel);
        }

        [HttpGet]
        [AllowAnonymous]
        [RestoreModelStateFromTempDataFilter]
        public async Task<ActionResult> GetForBooking([FromQuery]int housingId, int page = 1)
        {
            var errorModel = await _housingValidation.ValidateHousingToBooking(housingId);
            if (errorModel == null)
            {
                var housing = await _housingService.Get(housingId, page);
                var viewModel = _mapper.Map<HousingIndexDTO, HousingIndexViewModel>(housing);
                viewModel.Files = _fileStorageService.GetHousingFilesIfExist(housing.User.Id, housing.Id);

                return View(viewModel);
            }

            return View("~/Views/Error/Error.cshtml", errorModel);
        }

        [HttpGet]
        public async Task<ActionResult> GetUsersHousing([FromQuery]int housingId)
        {
            var housing = await _housingService.GetUsersHousing(housingId);
            var viewModel = _mapper.Map<UserHousingDTO, UserHousingViewModel>(housing);

            return View(viewModel);
        }

        [HttpDelete]
        [Route("{housingId}")]
        public async Task<ActionResult> Delete(int housingId)
        {
            int userId = User.Identity.GetUserId();
            var errorModel = await _housingValidation.ValidateHousingToDelete(housingId, userId);
            if (errorModel == null)
            {
                await _housingService.Delete(housingId);

                return Json(new { result = new ErrorModel(true) });
            }

            return Json(new { result = new ErrorModel(errorModel.Code, _localizer[errorModel.Message].Value) });
        }

        [HttpPost]
        [AllowAnonymous]
        [SetTempDataModelStateFilte]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Update(HousingUpdateViewModel updateModel)
        {
            int userId = User.Identity.GetUserId();
            updateModel.UserId = userId;

            if (updateModel.Id == 0)
            {
                var error = _fileStorageService.ValidateFilesBeforeAdd(updateModel);
                if(error != null)
                {
                    ViewData["error"] = error.Message;
                    return View("Update", updateModel);
                }

                var housingDTO = _mapper.Map<HousingUpdateViewModel, HousingCreateDTO>(updateModel);
                int housingId = await _housingService.CreateAndGetId(housingDTO);

                await _fileStorageService.SaveHousingFiles(userId, housingId, updateModel.AdditionalFiles);
                var mainUrl = await _fileStorageService.SaveMainPhoto(userId, housingId, updateModel.MainImg);
                
                await _housingService.AddMainPhoto(housingId, mainUrl);

                return RedirectToAction("GetUsersHousing", "Housing", new { housingId });
            }
            else
            {
                var updateDTO = _mapper.Map<HousingUpdateViewModel, HousingUpdateDTO>(updateModel);
                if(updateModel.MainImg != null)
                {
                    updateDTO.MainImgUrl = await _fileStorageService.SaveMainPhoto(userId, updateModel.Id, updateModel.MainImg);
                }

                await _housingService.Update(updateDTO);

                return RedirectToAction("GetUsersHousing", "Housing", new { housingId = updateDTO.Id });
            }
        }

        [HttpGet]
        [AllowAnonymous]
        [RestoreModelStateFromTempDataFilter]
        public async Task<ActionResult> Search(string headline, int page = 1)
        {
            var housesDTO = await _housingService.Search(headline, page);
            HousingSearchResultViewModel model = new()
            {
                SearchHeadline = headline,
                Houses = housesDTO,
                Page = page
            };

            return View("SearchResult", model);
        }

        [HttpGet]
        [AllowAnonymous]
        [RestoreModelStateFromTempDataFilter]
        public async Task<ActionResult> SearchFilter(string SearchHeadline, string Amount, bool Bathroom,
            bool WiFi, bool BathAccessories, bool Television, bool Conditioner, bool Kitchen, bool Wardrobe, bool Microwave,
          bool WashingMachine, int page = 1, int Adult = 0, int Child = 0, int SleepingPlaces = 0)
        {
            HousingSearchResultViewModel returnedModel = new();

            if (SearchHeadline != null)
            {
                var housesDTO = await _housingService.Search(SearchHeadline, page);
                returnedModel.SearchHeadline = SearchHeadline;
                returnedModel.Houses = housesDTO;
            }
            else
            {
                var housesDto = await _housingService.SearchFilter(Amount, Adult, Child, SleepingPlaces, Bathroom, WiFi, BathAccessories, Television, Conditioner,
                Kitchen, Wardrobe, Microwave, WashingMachine, page);
                returnedModel.Houses = housesDto;
                returnedModel.Amount = Amount;
            }

            returnedModel.Page = page;

            return View("SearchResult", returnedModel);
        }

        [HttpPost]
        [AllowAnonymous]
        [SetTempDataModelStateFilte]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Search(string headline)
        {
            var housesDTO = await _housingService.Search(headline, 1);
            HousingSearchResultViewModel model = new()
            {
                SearchHeadline = headline,
                Houses = housesDTO,
                Page = 1
            };
            
            return View("SearchResult", model);
        }
    }
}
