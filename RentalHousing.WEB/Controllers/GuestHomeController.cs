﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using BLL.DTO;
using BLL.Interfaces.IService;
using RentalHousing.WEB.Filters;
using RentalHousing.WEB.Models.Comment;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RentalHousing.WEB.Controllers
{
    [ServiceFilter(typeof(ExceptionFilter))]
    public class GuestHomeController : Controller
    {
        private readonly ICommentService _commentService;
        private readonly IMapper _mapper;

        public GuestHomeController(IMapper mapper, ICommentService commentService)
        {
            _mapper = mapper;
            _commentService = commentService;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            //User.Identity.GetUserId();
            var commentsDTO = await _commentService.GetCommentsForIndex();
            var commentsVM = _mapper.Map<IEnumerable<CommentHousingDTO>, IEnumerable<CommentWithUserVM>>(commentsDTO);

            return View(commentsVM);
        }

        [HttpGet]
        public IActionResult About()
        {
            return View();
        }
    }
}
