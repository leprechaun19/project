﻿using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using BLL.Interfaces.IService;
using RentalHousing.WEB.Models.Home;
using RentalHousing.WEB.Models.Comment;
using System.Collections.Generic;
using BLL.DTO;
using RentalHousing.WEB.Utils;
using RentalHousing.WEB.Filters;
using RentalHousing.WEB.Helpers;

namespace RentalHousing.WEB.Controllers
{
    [Authorize]
    [ServiceFilter(typeof(ExceptionFilter))]
    public class HomeController : Controller
    {
        private readonly ICommentService _commentService;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public HomeController(IUserService service, IMapper mapper, ICommentService commentService)
        {
            _mapper = mapper;
            _userService = service;
            _commentService = commentService;
        }

        [HttpGet]
        public async Task<IActionResult> DetermineRole()
        {
            int userId = User.Identity.GetUserId();
            var user = await _userService.GetPersonalProfile(userId);

            AccountHelper.DetermineRole(user.IsAdmin, out string nameController, out string nameMethod);

            return RedirectToAction(nameMethod, nameController);
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var commentsDTO = await _commentService.GetCommentsForIndex();
            var commentsVM = _mapper.Map<IEnumerable<CommentHousingDTO>,IEnumerable<CommentWithUserVM>>(commentsDTO);

            return View(commentsVM);
        }

        [HttpGet]
        public async Task<IActionResult> MyAds(int page = 1)
        {
            int userId = User.Identity.GetUserId();
            var user = await _userService.GetItemWithHouses(userId, page);
            var model = _mapper.Map<MyAdsViewModel>(user);
            model.Page = page;

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> MyFavourites( int page = 1)
        {
            int userId = User.Identity.GetUserId();
            var user = await _userService.GetItemWithFavourites(userId, page);
            var model = _mapper.Map<MyFavouritesViewModel>(user);
            model.Page = page;

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> MyBookings( int page = 1)
        {
            int userId = User.Identity.GetUserId();
            var user = await _userService.GetItemWithBookings(userId, page);
            var model = _mapper.Map<MyBookingsViewModel>(user);
            model.Page = page;

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> MyComments(int page = 1)
        {
            int userId = User.Identity.GetUserId();
            var user = await _userService.GetItemWithComments(userId, page);
            var model = _mapper.Map<MyCommentsViewModel>(user);
            return View(model);
        }

        [HttpGet]
        [ResponseCache(Duration = 300)]
        public IActionResult About()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Success()
        {
            return View();
        }
    }
}
