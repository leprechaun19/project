﻿using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using BLL.DTO;
using BLL.Interfaces.IService;
using BLL.Interfaces.Validation;
using Common;
using RentalHousing.WEB.Filters;
using RentalHousing.WEB.Models.User;
using RentalHousing.WEB.Utils;

namespace RentalHousing.WEB.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    [ServiceFilter(typeof(ExceptionFilter))]
    public class UserController : Controller
    {
        private readonly IUserValidation _userValidation;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public UserController(IUserService service, IMapper mapper, IUserValidation userValidation)
        {
            _mapper = mapper;
            _userService = service;
            _userValidation = userValidation;
        }

        [HttpGet]
        [Authorize(Roles = UserRoles.User)]
        public async Task<IActionResult> UserProfile([FromQuery]int userId, int pageHouses = 1)
        {
            var errorModel = await _userValidation.ValidateUserProfile(userId);
            if (errorModel == null)
            {
                var user = await _userService.GetUserProfile(userId, pageHouses);
                var viewModel = _mapper.Map<UserProfileDTO, UserProfileViewModel>(user);

                return View(viewModel);
            }

            return View("~/Views/Error/Error.cshtml", errorModel);
        }

        [HttpGet]
        [Authorize(Roles = UserRoles.User)]
        public async Task<IActionResult> PersonalProfile()
        {
            int userId = User.Identity.GetUserId();
            var user = await _userService.GetPersonalProfile(userId);
            var viewModel = _mapper.Map<UserUpdateDTO, UserUpdateViewModel>(user);

            return View(viewModel);
        }

        [HttpGet]
        [Authorize(Roles = UserRoles.User)]
        [RestoreModelStateFromTempDataFilter]
        public IActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [SetTempDataModelStateFilte]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = UserRoles.User)]
        public async Task<IActionResult> ChangePassword([FromForm]ChangePasswordViewModel model)
        {
            int id = User.Identity.GetUserId();

            var errorModel = await _userValidation.ValidateOldPassword(model.OldPassword, id);
            if(errorModel == null)
            {
                var changePasswordDTO = _mapper.Map<ChangePasswordDTO>(model);
                await _userService.ChangePassword(changePasswordDTO, id);

                ViewData["success"] = "ChangePasswordOk";
                return RedirectToAction("PersonalProfile");
            }
            else
            {
                ViewData["error"] = errorModel.Message;
                return View();
            }
        }

        [HttpGet]
        public async Task<ActionResult> Delete([FromQuery]int userId)
        {
            await _userService.Delete(userId);
            return View();
        }

        [HttpGet]
        [Authorize(Roles = UserRoles.User)]
        [RestoreModelStateFromTempDataFilter]
        public async Task<ActionResult> Update()
        {
            int userId = User.Identity.GetUserId();
            var user = await _userService.GetPersonalProfile(userId);
            var viewModel = _mapper.Map<UserUpdateDTO, UserUpdateViewModel>(user);

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [SetTempDataModelStateFilte]
        [Authorize(Roles = UserRoles.User)]
        public async Task<ActionResult> Update([FromForm]UserUpdateViewModel updateModel)
        {
            var updateDTO = _mapper.Map<UserUpdateViewModel, UserUpdateDTO>(updateModel);
            await _userService.UpdateItem(updateDTO);

            return RedirectToAction("PersonalProfile");
        }
    }
}
