﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using AutoMapper;
using RentalHousing.WEB.Filters;
using Common;

namespace RentalHousing.WEB.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    [ServiceFilter(typeof(ExceptionFilter))]
    public class AdminController : Controller
    {
        private readonly IMapper _mapper;

        public AdminController(IMapper mapper)
        {
            _mapper = mapper;
        }

        [HttpGet]
        [Authorize(Roles = UserRoles.Admin)]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [Authorize(Roles = UserRoles.Admin)]
        public IActionResult UserIndex()
        {
            return View("~/Views/User/Admin/Index.cshtml");
        }
    }
}
