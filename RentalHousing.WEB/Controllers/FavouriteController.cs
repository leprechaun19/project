﻿using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using BLL.DTO;
using BLL.Interfaces.IService;
using BLL.Interfaces.Validation;
using Common.Error;
using RentalHousing.WEB.Filters;
using RentalHousing.WEB.Models.Favourite;
using RentalHousing.WEB.Resources;
using RentalHousing.WEB.Utils;

namespace RentalHousing.WEB.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    [ServiceFilter(typeof(ExceptionFilter))]
    public class FavouriteController : Controller
    {
        private readonly SharedViewLocalizer _localizer;
        private readonly IFavouriteValidation _favouriteValidation;
        private readonly IFavouriteService _favouriteService;
        private readonly IMapper _mapper;

        public FavouriteController(IFavouriteService service, IMapper mapper, SharedViewLocalizer localizer, IFavouriteValidation favouriteValidation)
        {
            _mapper = mapper;
            _localizer = localizer;
            _favouriteService = service;
            _favouriteValidation = favouriteValidation;
        }

        [HttpPost]
        public async Task<ActionResult> Create([FromBody]FavouriteCreateViewModel model)
        {
            model.UserId = User.Identity.GetUserId();

            var error = await _favouriteValidation.ValidateToCreate(model.UserId, model.HousingId);
            if (error == null)
            {
                var favouriteDTO = _mapper.Map<FavouriteCreateViewModel, FavouriteCreateDTO>(model);
                await _favouriteService.Create(favouriteDTO);

                return Json(new { result = new ErrorModel(true) });
            }

            return Json(new { result = new ErrorModel(error.Code, _localizer[error.Message].Value) });
        }

        [HttpDelete]
        [Route("{favouriteId}")]
        public async Task<ActionResult> Delete(int favouriteId)
        {
            await _favouriteService.Delete(favouriteId);
            return Ok();
        }
    }
}
