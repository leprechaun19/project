﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using BLL.Interfaces.Validation;
using BLL.Interfaces.IService;
using System.Threading.Tasks;
using RentalHousing.WEB.Models.Account;
using AutoMapper;
using BLL.DTO;
using System;
using System.Security.Claims;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Http;
using RentalHousing.WEB.Filters;
using Microsoft.AspNetCore.Authentication.Cookies;
using BLL.Interfaces;
using RentalHousing.WEB.Resources;
using RentalHousing.WEB.Utils;
using Common;
using RentalHousing.WEB.Helpers;

namespace RentalHousing.WEB.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    [ServiceFilter(typeof(ExceptionFilter))]
    public class AccountController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IEmailSendingService _emailSendingService;
        private readonly IUserValidation _userValidation;
        private readonly IUserService _userService;

        private readonly SharedViewLocalizer _localizer;

        public AccountController(IUserService userService, IUserValidation userValidation, IMapper mapper,
            IEmailSendingService emailSendingService, SharedViewLocalizer localizer)
        {
            _localizer = localizer;
            _mapper = mapper;
            _emailSendingService = emailSendingService;
            _userValidation = userValidation;
            _userService = userService;
        }

        [HttpGet]
        [AllowAnonymous]
        [RestoreModelStateFromTempDataFilter]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ModelStateValidationFilter(RedirectToAction = "LoginErrorModelState", RedirectToController = "Account")]
        [ValidateAntiForgeryToken]
        [SetTempDataModelStateFilte]
        public async Task<IActionResult> Login([FromForm]LogInViewModel model)
        {
            var loginDTO = _mapper.Map<UserLogInDTO>(model);
            var error = await _userValidation.IsSignIn(loginDTO);
            if (error == null)
            {
                var user = await _userService.GetByLogin(model.UserEmail);
                await Authenticate(user);

                AccountHelper.DetermineRole(user.IsAdmin, out string nameController, out string nameMethod);

                return RedirectToAction(nameMethod, nameController);
            }
            else
            {
                ViewData["error"] = error.Message;
                return View(model);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Index", "Home");
        }


        [HttpGet]
        [AllowAnonymous]
        [RestoreModelStateFromTempDataFilter]
        public IActionResult Register(string returnUrl)
        {
            return View(new RegisterViewModel { ReturnUrl = returnUrl});
        }

        [HttpPost]
        [AllowAnonymous]
        [ModelStateValidationFilter(RedirectToAction = "RegisterErrorModelState", RedirectToController = "Account")]
        [ValidateAntiForgeryToken]
        [SetTempDataModelStateFilte]
        public async Task<IActionResult> Register([FromForm]RegisterViewModel model)
        {
            UserCreateDTO userDTO = _mapper.Map<UserCreateDTO>(model);
            var errorModel = await _userValidation.ValidateUsersRegisterData(userDTO);
            if (errorModel == null)
            {
                userDTO.Token = TokenGenerator.GenerateToken(userDTO, DateTime.UtcNow.AddDays(14));
                int userId = await _userService.CreateItem(userDTO);

                if (userId != 0)
                {
                    await _emailSendingService.SendEmailAsync(new EmailRecieverModel
                    {
                        Message = string.Format(_localizer["RegisterEmailMessage"], userDTO.UserName, userId, userDTO.Token),
                        RecieverAddress = model.Email,
                        Subject = string.Format(_localizer["RegisterEmailSubject"], userDTO.UserName)
                    });
                    

                    if (model.ReturnUrl == null)
                    {
                        return RedirectToAction("EmailConfirmation", new { userId });
                    }
                    else
                    {
                        return Redirect(model.ReturnUrl);
                    }
                }

            }
            ViewData["error"] = errorModel.Message;
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult EmailConfirmation(int userId)
        {
            return View(userId);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> EmailConfirmed(int userId, string token)
        {
            var error = await _userValidation.ValidateConfirmToken(userId, token);
            if (error != null)
            {
                return View(error);
            }
            else
            {
                await _userService.ConfirmEmail(userId);
                return View();
            }
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("{userId}")]
        public async Task<IActionResult> ResendMail(int userId)
        {
            var user = await _userService.GetPersonalProfile(userId);

            try
            {
                await _emailSendingService.SendEmailAsync(new EmailRecieverModel
                {
                    Message = string.Format(_localizer["RegisterEmailMessage"], user.UserName, userId, user.Token),
                    RecieverAddress = user.Email,
                    Subject = string.Format(_localizer["RegisterEmailSubject"], user.UserName)
                });
            }
            catch
            {
                return BadRequest();
            }

            return Ok();
        }


        [HttpGet]
        [AllowAnonymous]
        public IActionResult ChangeLocalization([FromQuery]string culture, string returnUrl)
        {
            Response.Cookies.Append(CookieRequestCultureProvider.DefaultCookieName,
            CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
            new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1), IsEssential = true });

            return LocalRedirect(returnUrl);
        }

        private async Task Authenticate(UserUpdateDTO user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Email, user.Email),
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Role, user.IsAdmin == true ? UserRoles.Admin : UserRoles.User)
            };

            ClaimsIdentity claimIdentity = new(claims, CookieAuthenticationDefaults.AuthenticationScheme);

            var authProperties = new AuthenticationProperties
            {
                AllowRefresh = true,
                ExpiresUtc = DateTimeOffset.Now.AddMonths(1),
                IsPersistent = true,
            };

            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, 
                new ClaimsPrincipal(claimIdentity), authProperties);
        }

        #region Errors

        [HttpGet]
        public IActionResult LoginErrorModelState()
        {
            ViewData["error"] = "InvalidModel";
            return View("Login");
        }

        [HttpGet]
        public IActionResult RegisterErrorModelState()
        {
            ViewData["error"] = "InvalidModel";
            return View("Register");
        }
        #endregion
    }
}
