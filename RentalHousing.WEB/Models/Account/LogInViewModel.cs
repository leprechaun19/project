﻿
using System;
using System.ComponentModel.DataAnnotations;

namespace RentalHousing.WEB.Models.Account
{
    [Serializable]
    public class LogInViewModel
    {
        [Required(ErrorMessage = "RequiredErrorMessage")]
        [EmailAddress(ErrorMessage = "EmailAddressErrorMessage")]
        [Display(Name = "Email")]
        [StringLength(100, MinimumLength = 6, ErrorMessage = "StringLengthErrorMessage")]
        public string UserEmail { get; set; }

        [Required(ErrorMessage = "RequiredErrorMessage")]
        [DataType(DataType.Password)]
        [StringLength(25, MinimumLength = 6, ErrorMessage = "StringLengthErrorMessage")]
        [Display(Name ="Password")]
        public string UserPassword { get; set; }
    }
}
