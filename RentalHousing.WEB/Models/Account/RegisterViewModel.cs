﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RentalHousing.WEB.Models.Account
{
    [Serializable]
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "RequiredErrorMessage")]
        [EmailAddress(ErrorMessage = "EmailAddressErrorMessage")]
        [StringLength(100, MinimumLength = 6, ErrorMessage= "StringLengthErrorMessage")]
        public string Email { get; set; }

        [Required(ErrorMessage = "RequiredErrorMessage")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "StringLengthErrorMessage")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "RequiredErrorMessage")]
        [Phone(ErrorMessage = "PhoneErrorMessage")]
        [StringLength(15, MinimumLength = 7, ErrorMessage = "StringLengthErrorMessage")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "RequiredErrorMessage")]
        [StringLength(25, MinimumLength = 6, ErrorMessage = "StringLengthErrorMessage")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "RequiredErrorMessage")]
        [StringLength(25, MinimumLength = 6, ErrorMessage = "StringLengthErrorMessage")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "CompareErrorMessage")]
        public string ConfirmPassword { get; set; }

        public string ReturnUrl { get; set; }
    }
}
