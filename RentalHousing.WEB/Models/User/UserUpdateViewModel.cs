﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RentalHousing.WEB.Models.User
{
    public class UserUpdateViewModel
    {
        [Required]
        public int Id { get; set; }

        [Required(ErrorMessage = "RequiredErrorMessage")]
        [EmailAddress(ErrorMessage = "EmailAddressErrorMessage")]
        [Display(Name = "EmailPlaceholder")]
        [StringLength(100, MinimumLength = 6, ErrorMessage = "StringLengthErrorMessage")]
        public string Email { get; set; }

        [Required(ErrorMessage = "RequiredErrorMessage")]
        [Display(Name = "UserNamePlaceholder")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "StringLengthErrorMessage")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "RequiredErrorMessage")]
        [Phone]
        [Display(Name = "PhonePlaceholder")]
        [StringLength(15, MinimumLength = 7, ErrorMessage = "StringLengthErrorMessage")]
        public string Phone { get; set; }

        [Display(Name = "DOBPlaceholder")]
        [DataType(DataType.Date)]
        public DateTime? DOB { get; set; }

        [Display(Name = "AddressPlaceholder")]
        [DataType(DataType.Text)]
        [StringLength(100, MinimumLength = 10, ErrorMessage = "StringLengthErrorMessage")]
        public string Address { get; set; }

        [Display(Name = "AboutMePlaceholder")]
        [DataType(DataType.Text)]
        [StringLength(300, MinimumLength = 6, ErrorMessage = "StringLengthErrorMessage")]
        public string AboutMe { get; set; }


        [Required]
        public bool EmailConfirmed { get; set; }
        [Required]
        public bool PhoneConfirmed { get; set; }
    }
}
