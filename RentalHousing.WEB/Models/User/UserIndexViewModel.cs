﻿using System;

namespace RentalHousing.WEB.Models.User
{
    public class UserIndexViewModel
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public DateTime? DOB { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }

        public DateTime CreationDate { get; set; }
        public bool Deleted { get; set; }
        public bool EmailConfirmed { get; set; }
        public bool PhoneConfirmed { get; set; }
    }
}
