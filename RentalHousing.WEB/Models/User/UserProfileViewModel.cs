﻿using BLL.DTO;
using System.Collections.Generic;

namespace RentalHousing.WEB.Models.User
{
    public class UserProfileViewModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }

        public string AboutMe { get; set; }
        public bool EmailConfirmed { get; set; }
        public bool PhoneConfirmed { get; set; }

        public IEnumerable<HousingInfoDTO> Houses { get; set; }
    }
}
