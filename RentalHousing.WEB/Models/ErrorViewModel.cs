using System;

namespace RentalHousing.WEB.Models
{
    public class ErrorViewModel
    {
        public string Message { get; set; }
    }
}