﻿using System;

namespace RentalHousing.WEB.Models.Comment
{
    public class CommentWithUserVM
    {
        public int UserId { get; set; }
        public string UserName { get; set; }

        public DateTime CreationDate { get; set; }

        public short Evaluation { get; set; }
        public string Comment { get; set; }
    }
}
