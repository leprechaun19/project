﻿using System.ComponentModel.DataAnnotations;

namespace RentalHousing.WEB.Models.Comment
{
    public class CommentCreateViewModel
    {
        public int Id { get; set; }
        [Required]
        public int HousingId { get; set; }

        public int BookingId { get; set; }
        public int UserId { get; set; }

        [Required]
        [Range(1, 5, ErrorMessage = "The {0} must be at least {2} and at max {1}.")]
        [Display(Name = "Evaluation")]
        public int Evaluation { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [StringLength(350, ErrorMessage = "The {0} must be at max {1} characters long.")]
        [Display(Name = "Comment")]
        public string Comment { get; set; }
    }
}
