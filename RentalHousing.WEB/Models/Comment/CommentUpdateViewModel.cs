﻿

using System.ComponentModel.DataAnnotations;

namespace RentalHousing.WEB.Models.Comment
{
    public class CommentUpdateViewModel
    {
        public int Id { get; set; }
        public int HousingId { get; set; }
        public int UserId { get; set; }

        public short Evaluation { get; set; }

        [Required(ErrorMessage = "RequiredErrorMessage")]
        [StringLength(350, MinimumLength = 3, ErrorMessage = "StringLengthErrorMessage")]
        public string Comment { get; set; }
    }
}
