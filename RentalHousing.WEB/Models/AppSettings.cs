﻿
namespace RentalHousing.WEB.Models
{
    public class AppSettings
    {
        public string GlobalSalt { get; set; }
        public int PageSize { get; set; }

        public string FileStoragePath { get; set; }
        public string FilesHousingPath { get; set; }
    }
}
