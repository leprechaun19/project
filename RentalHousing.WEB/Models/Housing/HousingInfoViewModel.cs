﻿using Common.Enums;
using System;

namespace RentalHousing.WEB.Models.Housing
{
    public class HousingInfoViewModel
    {
        public int Id { get; }

        public TypeOfHousing TypeOfHousing { get; set; }
        public string AdHeadline { get; set; }
        public string MainImgUrl { get; set; }
        public string Address { get; set; }
        public double PriceNight { get; set; }

        public DateTime CreationDate { get; }

        public bool WiFi { get; set; }
        public bool BathAccessories { get; set; }
        public bool Television { get; set; }
        public bool Conditioner { get; set; }
        public bool Kitchen { get; set; }
        public bool Wardrobe { get; set; }
        public bool Microwave { get; set; }
        public bool WashingMachine { get; set; }
    }
}
