﻿using BLL.DTO;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RentalHousing.WEB.Models.Housing
{
    public class HousingSearchResultViewModel
    {
        public string Amount { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Проверьте введенные данные.")]
        public int? Adult { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "Проверьте введенные данные.")]
        public int? Child { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Проверьте введенные данные.")]
        public int? SleepingPlaces { get; set; }

        public bool WiFi { get; set; }
        public bool BathAccessories { get; set; }
        public bool Bathroom { get; set; }
        public bool Television { get; set; }
        public bool Conditioner { get; set; }
        public bool Kitchen { get; set; }
        public bool Wardrobe { get; set; }
        public bool Microwave { get; set; }
        public bool WashingMachine { get; set; }
        public string SearchHeadline { get; set; }

        public int Page { get; set; }

        public IEnumerable<HousingInfoDTO> Houses {get; set;}
    }
}
