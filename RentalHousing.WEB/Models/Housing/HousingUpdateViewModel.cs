﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RentalHousing.WEB.Models.Housing
{
    public class HousingUpdateViewModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        

        [Required]
        [Display(Name = "TypeofHousing")]
        public int TypeOfHousing { get; set; }

        [Required]
        [Display(Name = "AdHeadlineLabel")]
        public string AdHeadline { get; set; }

        [Required]
        [Display(Name = "AddressLabel")]
        public string Address { get; set; }

        [Required]
        [Display(Name = "PriceNightLabel")]
        public double PriceNight { get; set; }

        [Required]
        [Display(Name = "Numberofpossibleguestslabel")]
        public int NumberOfPossibleGuests { get; set; }

        [Required]
        [Display(Name = "NumberOfBedsLabel")]
        public int NumberOfBeds { get; set; }

        [Required]
        [Display(Name = "SleepingPlacesLabel")]
        public int SleepingPlaces { get; set; }

        [Required]
        [Display(Name = "Bathroom")]
        public bool Bathroom { get; set; }

        [Required]
        [Display(Name = "WiFi")]
        public bool WiFi { get; set; }

        [Required]
        [Display(Name = "BathAccessories")]
        public bool BathAccessories { get; set; }

        [Required]
        [Display(Name = "Television")]
        public bool Television { get; set; }

        [Required]
        [Display(Name = "Conditioner")]
        public bool Conditioner { get; set; }

        [Required]
        [Display(Name = "Kitchen")]
        public bool Kitchen { get; set; }

        [Required]
        [Display(Name = "Wardrobe")]
        public bool Wardrobe { get; set; }

        [Required]
        [Display(Name = "Microwave")]
        public bool Microwave { get; set; }

        [Required]
        [Display(Name = "WashingMachine")]
        public bool WashingMachine { get; set; }

        [Display(Name = "AdditionalFiles")]
        public List<IFormFile> AdditionalFiles { get; set; }

        [Display(Name = "MainPhoto")]
        public IFormFile MainImg { get; set; }
    }
}
