﻿using BLL.DTO;
using Common.Enums;
using System.Collections.Generic;

namespace RentalHousing.WEB.Models.Housing
{
    public class UserHousingViewModel
    {
        public int Id { get; set; }

        public TypeOfHousing TypeOfHousing { get; set; }
        public string AdHeadline { get; set; }
        public string Address { get; set; }
        public double PriceNight { get; set; }

        public int NumberOfPossibleGuests { get; set; }
        public int NumberOfBeds { get; set; }
        public int SleepingPlaces { get; set; }
        public bool Bathroom { get; set; }

        public bool WiFi { get; set; }
        public bool BathAccessories { get; set; }
        public bool Television { get; set; }
        public bool Conditioner { get; set; }
        public bool Kitchen { get; set; }
        public bool Wardrobe { get; set; }
        public bool Microwave { get; set; }
        public bool WashingMachine { get; set; }

        public IEnumerable<ScheduleOfBookingDTO> Schedules { get; set; }
    }
}
