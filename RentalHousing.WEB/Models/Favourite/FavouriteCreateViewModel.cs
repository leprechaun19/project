﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RentalHousing.WEB.Models.Favourite
{
    public class FavouriteCreateViewModel
    {
        public int UserId { get; set; }
        public int HousingId { get; set; }
    }
}
