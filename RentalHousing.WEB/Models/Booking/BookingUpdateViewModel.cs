﻿using Common.Enums;
using System.ComponentModel.DataAnnotations;

namespace RentalHousing.WEB.Models.Booking
{
    public class BookingUpdateViewModel
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public BookingStatus Status { get; set; }
    }
}
