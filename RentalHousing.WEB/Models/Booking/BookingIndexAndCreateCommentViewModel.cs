﻿using RentalHousing.WEB.Models.Comment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RentalHousing.WEB.Models.Booking
{
    public class BookingIndexAndCreateCommentViewModel
    {
        public BookingIndexViewModel Booking { get; set; }
        public CommentCreateViewModel Comment { get; set; }
    }
}
