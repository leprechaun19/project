﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RentalHousing.WEB.Models.Booking
{
    public class BookingCreateViewModel
    {
        public int UserId { get; set; }
        [Required]
        public int HousingId { get; set; }

        [Required]
        [Display(Name = "Start Date")]
        public DateTime StartDate { get; set; }

        [Required]
        [Display(Name = "End Date")]
        public DateTime EndDate { get; set; }

        [Required]
        [Range(1, 10, ErrorMessage = "The {0} must be at least {2} and at max {1}.")]
        [Display(Name = "Adults")]
        public int Adults { get; set; }

        [Required]
        public double Price { get; set; }

        [Required]
        [Range(0, 5, ErrorMessage = "The {0} must be at least {2} and at max {1}.")]
        [Display(Name = "Children from 6")]
        public int ChildrenFrom6To17 { get; set; }

    }
}
