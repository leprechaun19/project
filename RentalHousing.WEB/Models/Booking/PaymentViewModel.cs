﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RentalHousing.WEB.Models.Booking
{
    public class PaymentViewModel
    {
        public int BookingId { get; set; }

        public double Cost { get; set; }
    }
}
