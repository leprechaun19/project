﻿using Common.Enums;
using RentalHousing.WEB.Models.Comment;
using RentalHousing.WEB.Models.Housing;
using System;

namespace RentalHousing.WEB.Models.Booking
{
    public class BookingIndexViewModel
    {
        public int Id { get; set; }
         
        public int HousingId { get; set; }
        public CommentUpdateViewModel Comment { get; set; }
        public HousingInfoViewModel Housing { get; set; }

        public BookingStatus Status { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CreationDate { get; }

        public double Cost { get; set; }

        public int Adults { get; set; }
        public int ChildrenFrom6To17 { get; set; }
    }
}
