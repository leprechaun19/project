﻿using BLL.DTO;
using System.Collections.Generic;

namespace RentalHousing.WEB.Models.Home
{
    public class MyCommentsViewModel
    {
        public int Id { get; }

        public IEnumerable<CommentProfileDTO> Comments { get; }
     
        public MyCommentsViewModel(int id, IEnumerable<CommentProfileDTO> comments)
        {
            Id = id;
            Comments = comments;

        }
    }
}
