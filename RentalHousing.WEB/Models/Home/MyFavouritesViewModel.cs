﻿using BLL.DTO;
using System.Collections.Generic;

namespace RentalHousing.WEB.Models.Home
{
    public class MyFavouritesViewModel
    {
        public int Id { get; set; }
        public int Page { get; set; }

        public IEnumerable<FavouriteIndexDTO> Favourites { get; set; }

        public MyFavouritesViewModel(int id, IEnumerable<FavouriteIndexDTO> favourites)
        {
            Id = id;
            Favourites = favourites;

        }
    }
}
