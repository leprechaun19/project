﻿using BLL.DTO;
using System.Collections.Generic;

namespace RentalHousing.WEB.Models.Home
{
    public class MyAdsViewModel
    {
        public int Id { get; set; }
        public int Page { get; set; }

        public IEnumerable<HousingInfoDTO> Houses { get; set; }

        public MyAdsViewModel(int id, IEnumerable<HousingInfoDTO> houses)
        {
            Id = id;
            Houses = houses;

        }
    }
}
