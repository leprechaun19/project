﻿using BLL.DTO;
using System.Collections.Generic;

namespace RentalHousing.WEB.Models.Home
{
    public class MyBookingsViewModel
    {
        public int Id { get; set; }

        public int Page { get; set; }
        public IEnumerable<BookingInfoDTO> Bookings { get; set; }

        public MyBookingsViewModel(int id, IEnumerable<BookingInfoDTO> bookings)
        {
            Id = id;
            Bookings = bookings;

        }
    }
}
