﻿using Microsoft.Extensions.Localization;
using System.Reflection;

namespace RentalHousing.WEB.Resources
{
    public class SharedViewLocalizer
    {
        private readonly IStringLocalizer _localizer;

        public SharedViewLocalizer(IStringLocalizerFactory factory)
        {
            var type = typeof(SharedResource);
            string assembleName = type.GetTypeInfo().Assembly.FullName;
            _localizer = factory.Create("SharedResource", assembleName);
        }

        public LocalizedString this[string key] => _localizer[key];

        public LocalizedString GetLocalizedString(string key)
        {
            return _localizer[key];
        }
    }
}
