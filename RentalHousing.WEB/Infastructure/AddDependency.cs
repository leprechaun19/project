﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using BLL.Interfaces;
using BLL.Interfaces.IService;
using BLL.Interfaces.Validation;
using BLL.Services;
using RentalHousing.BLL.Services.Validation;
using Common.Interfaces;
using Common.Models;
using Common.Password;
using RentalHousing.WEB.Filters;
using RentalHousing.WEB.Interfaces;
using RentalHousing.WEB.Models;
using RentalHousing.WEB.Resources;
using RentalHousing.WEB.Services;
using BLL.Services.Validation;

namespace RentalHousing.WEB.Infastructure
{
    public static class AddDependency
    {
        public static IServiceCollection AddBLLServices(this IServiceCollection services, string globalSalt, SMTPSettings smtpSettings)
        {
            services.AddScoped<ICommentValidation, CommentValidation>();
            services.AddScoped<IHousingValidation, HousingValidation>();
            services.AddScoped<IUserValidation, UserValidation>();
            services.AddScoped<IBookingValidation, BookingValidation>();
            services.AddScoped<IFavouriteValidation, FavouriteValidation>();
            services.AddScoped<IBookingService, BookingService>();
            services.AddScoped<ICommentService, CommentService>();
            services.AddScoped<IFavouriteService, FavouriteService>();
            services.AddScoped<IHousingService, HousingService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IEmailSendingService>(ser => new EmailSendingService(smtpSettings));
            services.AddScoped<IPasswordManager>(s => new PasswordManager(globalSalt));;

            return services;
        }

        public static IServiceCollection AddPLServices(this IServiceCollection services, IWebHostEnvironment env, AppSettings settings)
        {
            services.AddScoped<SharedViewLocalizer>();
            services.AddScoped<ExceptionFilter>();
            services.AddScoped<IFileStorageService>(s => new FileStorageService(env, settings.FileStoragePath, settings.FilesHousingPath));
            services.AddTransient(s => s.GetService<IHttpContextAccessor>().HttpContext.User);

            return services;
        }
    }
}
