﻿

namespace RentalHousing.WEB.Helpers
{
    internal static class AccountHelper
    {
        /// <summary>
        /// The method determines which action to redirect the user to, depending on the value of the IsAdmin field.
        /// </summary>
        /// <param name="isAdmin">The value of the IsAdmin field.</param>
        /// <param name="nameController">The name of controller, where the user will be redirected.</param>
        /// <param name="nameMethod">The name of method, where the user will be redirected.</param>
        internal static void DetermineRole(bool isAdmin, out string nameController, out string nameMethod)
        {
            if (isAdmin)
            {
                nameController = "Admin";
                nameMethod = "Index";
            }
            else
            {
                nameController = "Home";
                nameMethod = "Index";
            }
        }
    }
}
